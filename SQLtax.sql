
CREATE TABLE `ps_tax_rules_group` (
  `id_tax_rules_group` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `active` int(11) NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_tax_rules_group`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


INSERT INTO `ps_tax_rules_group` VALUES (1, 'IVA (14%)', 1, 0, '2016-09-16 11:23:19', '2016-09-29 10:27:40');
INSERT INTO `ps_tax_rules_group` VALUES (2, 'Excepto IVA', 1, 0, '2016-09-29 10:27:58', '2016-09-29 10:28:52');

CREATE TABLE `ps_tax_rule` (
  `id_tax_rule` int(11) NOT NULL AUTO_INCREMENT,
  `id_tax_rules_group` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_state` int(11) NOT NULL,
  `zipcode_from` varchar(12) NOT NULL,
  `zipcode_to` varchar(12) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `behavior` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id_tax_rule`),
  KEY `id_tax_rules_group` (`id_tax_rules_group`),
  KEY `id_tax` (`id_tax`),
  KEY `category_getproducts` (`id_tax_rules_group`,`id_country`,`id_state`,`zipcode_from`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


INSERT INTO `ps_tax_rule` VALUES (1, 1, 81, 0, '0', '0', 1, 0, '');
INSERT INTO `ps_tax_rule` VALUES (2, 2, 81, 0, '0', '0', 2, 0, '');

CREATE TABLE `ps_tax_lang` (
  `id_tax` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_tax`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `ps_tax_lang` VALUES (1, 1, 'IVA 14%');
INSERT INTO `ps_tax_lang` VALUES (2, 1, 'Excepto de IVA');

CREATE TABLE `ps_tax` (
  `id_tax` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rate` decimal(10,3) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_tax`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


INSERT INTO `ps_tax` VALUES (1, 14.000, 1, 0);
INSERT INTO `ps_tax` VALUES (2, 0.000, 1, 0);
-- -----------
-- JOINS que permiten obtener los datos del impuesto
Select pp.id_product, pt.id_tax, ptrg.id_tax_rules_group, name, rate from ps_tax_rules_group as ptrg join ps_tax_rule as ptr on ptrg.id_tax_rules_group = ptr.id_tax_rules_group join ps_tax as pt on pt.id_tax = ptr.id_tax join ps_product as pp on pp.id_tax_rules_group = ptrg.id_tax_rules_group where pt.active = 1;

-- JOINS que permiten obtener los datos de las descripciones cortas y largas
SELECT  id_product, position, `value` from ps_feature as pf left join ps_feature_lang as pfl on pf.id_feature = pfl.id_feature left join ps_feature_product as pfp on pf.id_feature = pfp.id_feature join ps_feature_value as pfv on pfv.id_feature = pf.id_feature join ps_feature_value_lang as pfvl on pfvl.id_feature_value = pfp.id_feature_value where pfp.id_feature_value = pfv.id_feature_value

-- JOINs que permiten obtener los datos de la tabla de productos (excepto las imagenes)
--Para los que si tienen impuesto
SELECT pp.id_product as id, ppl.name as nombre, reference as referencia, pcl.name as categoria, price as preciobase, price*1.12 as preciofinal, psa.quantity as cantidad, ppl.description as descripcion, ppl.description_short  as descripcioncorta from ps_product as pp join ps_product_lang as ppl on pp.id_product = ppl.id_product join ps_stock_available as psa on psa.id_product = pp.id_product join ps_category_lang as pcl on pcl.id_category = pp.id_category_default join ps_tax_rules_group as ptrg on pp.id_tax_rules_group = ptrg.id_tax_rules_group where pp.id_tax_rules_group = 1



--Para los que no tienen impuesto
SELECT pp.id_product as id, ppl.name as nombre, reference as referencia, pcl.name as categoria, price as preciobase, price as preciofinal, psa.quantity as cantidad, ppl.description as descripcion, ppl.description_short  as descripcioncorta from ps_product as pp join ps_product_lang as ppl on pp.id_product = ppl.id_product join ps_stock_available as psa on psa.id_product = pp.id_product join ps_category_lang as pcl on pcl.id_category = pp.id_category_default join ps_tax_rules_group as ptrg on pp.id_tax_rules_group = ptrg.id_tax_rules_group where pp.id_tax_rules_group = 2


