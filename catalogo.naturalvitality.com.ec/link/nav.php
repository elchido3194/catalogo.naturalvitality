<?php
$hom1='';
$pro1='';
$usu1='';
$mis1='';
switch ($_SESSION['ubix']){
    case 1:
        $hom1 = "active";
        break;
    case 2:
        $pro1 = "active";
        break;
    case 3:
        $usu1 = "active";
        break;
    case 4:
        $mis1 = "active";
        break;
}
?>
<nav class="navbar fixed-top navbar-expand-md navbar-light bg-white">
    <a class="navbar-brand" href="<?php echo $_SESSION['nivelcap'];?>"><img src="//<?php echo ROOT_PATH_PHP;?>img/logo/logosmall.png" style="max-height: 50px;"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
<?php if($_SESSION['permisos']==2) : ?>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav text-center ml-auto">
            <li class="nav-item">
                <p class="nav-link" ><b>Usuario</b>: <?php echo $_SESSION['username']; ?> | <?php echo date("d/m/Y H:i:s"); ?></p>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php echo $hom1;?>" href="<?php echo '//'.ROOT_PATH_PHP;?>mainpag.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php echo $pro1;?>" href="<?php echo '//'.ROOT_PATH_PHP;?>productos/">Productos</a>
            </li>
        
            <li class="nav-item">
                <a class="nav-link <?php echo $mis1;?>" href="<?php echo '//'.ROOT_PATH_PHP;?>miscelaneos/">Misceláneos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo '//'.ROOT_PATH_PHP;?>session/cerrarSession.php">Cerrar Sesión</a>
            </li>
        </ul>
    </div>
<?php elseif($_SESSION['permisos']==1) :?>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav text-center ml-auto">
            <li class="nav-item">
                <p class="nav-link" ><b>Usuario</b>: <?php echo $_SESSION['username']; ?> | <?php echo date("d/m/Y H:i:s"); ?></p>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php echo $hom1;?>" href="<?php echo '//'.ROOT_PATH_PHP;?>mainpag.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php echo $pro1;?>" href="<?php echo '//'.ROOT_PATH_PHP;?>productos/">Productos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php echo $mis1;?>" href="<?php echo '//'.ROOT_PATH_PHP;?>miscelaneos/cambiarContra.php">Cambiar Contraseña</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo '//'.ROOT_PATH_PHP;?>session/cerrarSession.php">Cerrar Sesión</a>
            </li>
        </ul>
    </div>
<?php endif; ?>
</nav>
