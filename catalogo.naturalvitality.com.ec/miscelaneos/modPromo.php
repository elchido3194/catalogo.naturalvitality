<?php 
require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'/function/global.php');
require_once(ROOT_PATH_HTML.'/link/db.php');$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Modificación de Promociones';
$_SESSION['ubix'] = 4;
if ($_SESSION['permisos']==1){
	$lugar = "Location://".ROOT_PATH_PHP;
	header($lugar);
}

?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catalogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/dropzone.css">
        
	</head>
		<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');?>
		<?php
		//$globalid = ;

		if (isset($_POST['hidformcontrol'])){
			$id = $_POST['hidformcontrol'];
			$titulopromo =  $_POST['nombrepromo'];
			$descripcion = $_POST['descripcion'];
			$disclaimer = $_POST['disclaimer'];
			$fechainicio = $_POST['fechainicio'];
			$fechafin = $_POST['fechafin'];
			$username = $_SESSION['username'];	
			$modal = $_POST['selmodal'];
			$estado = 'activo';
			if(file_exists($_FILES['ima']['tmp_name'])){
				$nombreImagen = $_FILES['ima']['name'];
				$ext = pathinfo($nombreImagen, PATHINFO_EXTENSION);
				$nombrepromo = "Promocion_".$id;
				$imagen = $nombrepromo.'.'.$ext;
				$move = __DIR__.'/imgs/'.$imagen;
				//unlink($move);
				$quer = "Update promociones set titulo = ?, descripcion = ?, disclaimer = ?, modal = ?, inicio = STR_TO_DATE(?, '%d-%m-%Y'), fin = STR_TO_DATE(?, '%d-%m-%Y'), user = ?, imagen = ?, estado = ? where id = ".$id;
				$sq = $conn->prepare($quer);
				$sq ->bind_param("sssssssss", $titulopromo, $descripcion, $disclaimer, $modal, $fechainicio, $fechafin, $username, $imagen, $estado);

				move_uploaded_file($_FILES['ima']['tmp_name'], $move);

			}else{
				$quer = "Update promociones set titulo = ?, descripcion = ?, disclaimer = ?, modal = ?, inicio = STR_TO_DATE(?, '%d-%m-%Y'), fin = STR_TO_DATE(?, '%d-%m-%Y'), user = ?, estado = ? where id = ".$id;
				$sq = $conn->prepare($quer);
				$sq ->bind_param("ssssssss", $titulopromo, $descripcion, $disclaimer, $modal, $fechainicio, $fechafin, $username, $estado);
			}
			if($sq->execute()){
				echo "<script> alert('Promocion modificada correctamente');
				window.location.href='//".ROOT_PATH_PHP."miscelaneos';
			    	  </script>";
			}
		}

		if (isset($_GET['id'])){

			$id = $_GET['id'];
			$queryllenar = "Select nombre, titulo, descripcion, disclaimer, modal, DATE_FORMAT(inicio, '%d-%m-%Y') as inicio, DATE_FORMAT(fin, '%d-%m-%Y') as fin, imagen from promociones where id = '".$id."';";
			$resul = $conn->query($queryllenar);
			$row = $resul->fetch_assoc();
			
			//$query = "Insert into promociones (nombre, titulo, descripcion, disclaimer, modal, inicio, fin, user, ultimaModificacion, creacionRegistro, estado) values (?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'activo')";
			//$sql = $conn->prepare($query);
			//$sql->bind_param("ssssssss", $titulopromo, $titulopromo, $descripcion, $disclaimer, $modal, $fechainicio, $fechafin, $username); 
			
			//if ($sql->execute()){
			//	echo "<script> alert('Si funciona')	
			//	      </script>";
			//}
			//echo $nombrepromo."\n";
			//echo $descripcion."\n";
			//echo $disclaimer."\n";
			//echo $fechainicio."\n";
			//echo $fechafin."\n";

		}

		?>
		<section class="supertop">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<h3>Modificacion de Promociones</h3>
						<hr>
					<form method="POST" enctype="multipart/form-data">
						<div class="form-group col-12">
							<label for="nombrepromo">Nombre de la Promoción </label>
							<input type="text" class="form-control" id="nombrepromo" name="nombrepromo" placeholder="Nombre Promo" pattern="[0-9a-zA-Zñáéíóú\s]+" title="Solo letras" value="<?php echo $row['titulo'];?>" required>
							<input type="hidden" name="hidformcontrol" value="<?php echo $id;?>">
						</div>
						<div class="form-group col-12 text-center" style="border: 5px dashed #AED7FF">
							<img src="imgs/<?php echo $row['imagen']."?".$current_date;?>" id="blah" width="500" height="250">
							<input id="inputimagen" style="width: 100%" type="file" name="ima" accept="image/*" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" ondragover='changeColor(event)'>
						</div>
						<!--div class="dropzone dropzone-previews col-8 text-center col-md-auto" style="height: 200px" id="myDropzone"></div-->
						<br>
						<div class="form-group col-12">
							<label for="descripcion">Descripción </label>
							<textarea type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripción" pattern="[a-zA-Zñáéíóú\s]+" title="Solo letras" cols="100" rows="10"><?php echo $row['descripcion'];?></textarea>
						</div>
						<div class="form-group col-12">
							<label for="disclaimer">Disclaimer </label>
							<input type="text" class="form-control" id="disclaimer" name="disclaimer" placeholder="Disclaimer" pattern="[0-9a-zA-Zñáéíóú\s]+" value="<?php echo $row['disclaimer'];?>">
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label for="fechainicio">Fecha de Inicio</label>
								<input type="text" class="form-control" id="datepickerInicio" name="fechainicio" value="<?php echo $row['inicio'];?>" required>
							</div>
							<div class="form-group col-6">
								<label for="fechafin">Fecha Final </label>
								<input type="text" class="form-control" id="datepickerFin" name="fechafin" value="<?php echo $row['fin'];?>" required>
							</div>
						</div>
							<div class="form-group col-6">
								<label for="selmodal"> Desea que la promocion aparezca en el Pop-up inicial </label>
								<select class="custom-select lg-2 mb-2 mr-sm-2 mb-sm-0 form-control" id="selmodal" name="selmodal" required>
									<?php
										if($row['modal']=="si"){
											$si = "selected='selected'";
											$no = "";
										}else{
											$si = "";
											$no = "selected='selected'";
										}
										?>
									<option <?php echo $si; ?>> Si </option>
									<option <?php echo $no; ?>> No </option>
								</select>
							</div>
							<div class="col-12"> 
								<button type="submit" class="btn btn-info"> Modificar Promoción </button>
							</div>
					</form>
					
					</div>
					<div class="col-12 text-right">
						<a href='./cargaPromociones.php' class='btn btn-danger btn-sm' role='button' aria-disabled='true'>Regresar <i class='fa fa-reply' aria-hidden='true'></i></a>
					</div>
				</div>
			</div>
			<br>
		</section>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/dropzone.js"></script>
		<script>
		$( function() {
			$("#datepickerInicio").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: "dd-mm-yy"
			});
		});
		$( function() {
			$("#datepickerFin").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: "dd-mm-yy"
			});
		});
			$ ( function(){
			Dropzone.autoDiscover = false;
			Dropzone.options.myDropzone = {
				url: "yourUrl",
    			thumbnailWidth: null,
    			thumbnailHeight: null,
			dictDefaultMessage: "Haga click o arrastre para subir la imagen de la promocion",
    			maxFiles: 1,
   				init: function() {
        			this.on("thumbnail", function(file, dataUrl) {
            			$('.dz-image').last().find('img').attr({width: '', height: '220px'});
        			})

        			var prevFile;
            		this.on('addedfile', function() {
                	if (typeof prevFile !== "undefined") {
                    	this.removeFile(prevFile);
                	}
            		})

            		this.on('success', function(file, responseText) {
                		prevFile = file;
            		})
        		},

        			//this.on("success", function(file) {
            		//	$('.dz-image').css({"width":"100%", "height":"300px"});
        			//})
			};

			var myDropzone = new Dropzone('div#myDropzone');
			});
		</script>
	</body>
</html>