<?php 
require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Borrar Promociones';
$_SESSION['ubix'] = 4;

if ($_SESSION['permisos']==1){
	$lugar = "Location://".ROOT_PATH_PHP;
	header($lugar);
}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catalogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">

	</head>
	<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');?>
		<section class="supertop">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<h3>Esta seguro que desea desactivar la promoción?</h3>
					</div>

					<?php
						if (isset($_GET['id'])){	
							$id = $_GET['id'];
							$queryllenar = "Select nombre, titulo, descripcion, disclaimer, modal, inicio, fin, imagen from promociones where id = '".$id."';";
							$resul = $conn->query($queryllenar);
							$row = $resul->fetch_assoc();
						}

						if (isset($_POST['hidformcontrol'])){
							$id = $_POST['hidformcontrol'];
							$estado = "inactivo";
							$quer = "Update promociones set estado = ? where id = ".$id;
							$sq = $conn->prepare($quer);
							$sq ->bind_param("s", $estado);
							if ($sq->execute()){
								echo "<script> alert('La promocion ha sido desactivada');
								window.location.href='//".ROOT_PATH_PHP."miscelaneos';
			    	  			</script>";
							}

						}
					?>
					<form method="POST">
						<br>
						<div class="col-12"> 
								<button type="submit" class="btn btn-danger"> Desactivar Promoción </button>
						</div>
						<br>
						<div class="form-group col-12">
							<label for="nombrepromo">Nombre de la Promoción </label>
							<input type="text" class="form-control" id="nombrepromo" name="nombrepromo" placeholder="Nombre Promo" pattern="[0-9a-zA-Záéíóú\s]+" title="Solo letras" value="<?php echo $row['titulo'];?>" disabled>
							<input type="hidden" name="hidformcontrol" value="<?php echo $id;?>">
						</div>
						<br>
						<div class="form-group col-12">
							<label for="descripcion">Descripción </label>
							<textarea type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripción" pattern="[a-zA-Záéíóú\s]+" title="Solo letras" cols="100" rows="10" disabled=""><?php echo $row['descripcion'];?></textarea>
						</div>
						<div class="form-group col-12">
							<label for="disclaimer">Disclaimer </label>
							<input type="text" class="form-control" id="disclaimer" name="disclaimer" placeholder="Disclaimer" pattern="[0-9a-zA-Záéíóú\s]+" value="<?php echo $row['disclaimer'];?>" disabled>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label for="fechainicio">Fecha de Inicio</label>
								<input type="text" class="form-control" id="datepickerInicio" name="fechainicio" value="<?php echo $row['inicio'];?>" disabled>
							</div>
							<div class="form-group col-6">
								<label for="fechafin">Fecha Final </label>
								<input type="text" class="form-control" id="datepickerFin" name="fechafin" value="<?php echo $row['fin'];?>" disabled>
							</div>
						</div>
							<div class="form-group col-6">
								<label for="selmodal"> Desea que la promocion aparezca en el Pop-up inicial </label>
								<select class="custom-select lg-2 mb-2 mr-sm-2 mb-sm-0 form-control" id="selmodal" name="selmodal" disabled="">
									<?php
										if($row['modal']=="si"){
											$si = "selected='selected'";
											$no = "";
										}else{
											$si = "";
											$no = "selected='selected'";
										}
										?>
									<option <?php echo $si; ?>> Si </option>
									<option <?php echo $no; ?>> No </option>
								</select>
							</div>
							
					</form>
					<div class="col-12 text-right">
						<a href='./cargaPromociones.php' class='btn btn-outline-danger btn-sm' role='button' aria-disabled='true'>Regresar <i class='fa fa-reply' aria-hidden='true'></i></a>
					</div>
				</div>
			</div>

			<br>
		</section>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
	</body>
</html>
