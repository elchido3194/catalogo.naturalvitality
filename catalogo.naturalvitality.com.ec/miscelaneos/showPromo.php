<?php 
require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'/function/redirect.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Promociones';
$_SESSION['ubix'] = 4;
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catalogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<style type="text/css">
            .supertop{
                min-height: 90vh;
			}
					
			.fa{
				color: #30309A;
			}
			.fa-reply{
				color: white;
			}
			#carouselExampleControls{
				//background: linear-gradient(to right, white, white, blue, blue, white, white);
				background: #f6f6f6;
			}
			.d-block{
				max-width: 550px;
				margin: auto;
				padding: 15px;
			}
			
		</style>
	</head>
	<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');?>
		<div class='divpaginacompleta'>
		<section class="supertop text-center">
            <br>
			<div class="col-1">
				<a class="btn btn-danger" href="../">Regresar <i class="fa fa-reply" aria-hidden="true"></i></a>
			</div>
            <h1> Descripción de la Promoción</h1>
            <hr>
			<section class="showproducto">
				<div class="row justify-content-center align-items-center">
                   
					<?php
					if (isset($_GET['id'])){
						$id = $_GET['id'];
						$sql = "Select * from promociones where id = '$id'";
						$resul = $conn->query($sql);

						while ($row = $resul->fetch_assoc()) {

                        echo "<div class='col-12 col-md-8'><img src='imgs/".$row['imagen']."' class='img-fluid'></div></div>
                        <br>
                        <hr>
                        <div class='row justify-content-center'>
						<div class='col-12 col-md-8 text-center'>
							<h1>".$row['titulo']."</h1>
							
							<br>
							<tagdes>".$row['descripcion']."</tagdes>
                            <br><br>
                            <tagred> ".$row['disclaimer']."</tagred>
                            <br><br>
                        <div class='row'>
                        <div class='col-12 col-md-6'><tagfecha> Fecha de Inicio: ".$row['inicio']."</tagfecha></div>
                        <div class='col-12 col-md-6'><tagfecha> Fecha Final: ".$row['fin']."</tagfecha></div>
                        </div><hr>
						</div>";
					}
				}
			?>
			</div>
            </section>
			<h3>Otras Promociones</h3>
			<br>
			<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  				<div class="carousel-inner">
					   <?php 
						   $sql = "SELECT * FROM promociones where estado='activo'";
						   $resul = $conn->query($sql);
						   $row = [];
						   while($varia = $resul->fetch_assoc()){
							   $row[]=$varia;
						   }
							foreach($row as $prom){
								echo "<a href='showPromo.php?id=".$prom['id']."' class='carousel-item'><img class='d-block' src='./imgs/".$prom['imagen']."?".$current_date." alt='".$prom['titulo']."'></a>";
							}
					   ?>
  				</div>
				<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
   					<span class="fa fa-chevron-left" aria-hidden="true"></span>
    				<span class="sr-only">Previous</span>
  				</a>
 				<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    				<span class="fa fa-chevron-right" aria-hidden="true"></span>
    				<span class="sr-only">Next</span>
 				</a>
			</div>

            </section>
            </div>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script>
		$(document).ready(function () {
  			$('#carouselExampleControls').find('.carousel-item').first().addClass('active');
		});
		</script>
	</body>
</html>
