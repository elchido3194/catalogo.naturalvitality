<?php 
require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'/function/global.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Nuevas Promociones';
$_SESSION['ubix'] = 4;
if ($_SESSION['permisos']==1){
	$lugar = "Location://".ROOT_PATH_PHP;
	header($lugar);
}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catalogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/dropzone.css">
		<style>
			place {
				color: grey;
			}
		</style>
        
	</head>
		<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');?>
		<?php
		if (isset($_POST['hidformcontrol'])){
			$titulopromo =  $_POST['nombrepromo'];
			$descripcion = $_POST['descripcion'];
			$disclaimer = $_POST['disclaimer'];
			$fechainicio = $_POST['fechainicio'];
			$fechafin = $_POST['fechafin'];
			$username = $_SESSION['username'];	
			$modal = $_POST['selmodal'];
			$nombreImagen = $_FILES['ima']['name'];
			$ext = pathinfo($nombreImagen, PATHINFO_EXTENSION);

			$query = "Insert into promociones (nombre, titulo, descripcion, disclaimer, modal, inicio, fin, user, ultimaModificacion, creacionRegistro, estado) values (?,?,?,?,?,STR_TO_DATE(?, '%d-%m-%Y'),STR_TO_DATE(?, '%d-%m-%Y'),?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'activo')";
			$sql = $conn->prepare($query);
			$sql->bind_param("ssssssss", $titulopromo, $titulopromo, $descripcion, $disclaimer, $modal, $fechainicio, $fechafin, $username); 
			
			if ($sql->execute()){
				$que = "Select id from promociones where nombre = '".$titulopromo."';";
				$resul = $conn->query($que);
				$idprom = $resul->fetch_assoc();
				$idnum = $idprom['id'];
				$nombrepromo = "Promocion_".$idnum;
				$imagen = $nombrepromo.'.'.$ext;
				$move = __DIR__.'/imgs/'.$imagen;
				move_uploaded_file($_FILES['ima']['tmp_name'], $move);

				$quer = "Update promociones set imagen = ?, nombre = ? where id = ".$idnum;
				$sq = $conn->prepare($quer);
				$sq ->bind_param("ss", $imagen, $nombrepromo);
				if($sq->execute()){
					echo "<script> alert('Ingreso Correcto');
				    	  </script>";
				  }
			}else{
				echo "<script> alert('No se pudo ingresar el registro. Por favor verifique el nombre de la promocion e intentelo de nuevo');
				    	  </script>";
			}
		}
		?>
		<section class="supertop">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<h3>Nueva Promoción</h3>
						<hr>
					<form method="POST" enctype="multipart/form-data">
						<div class="form-group col-12">
							<label for="nombrepromo">Nombre de la Promoción </label>
							<input type="text" class="form-control" id="nombrepromo" name="nombrepromo" placeholder="Ingrese el nombre de la promoción" pattern="[0-9a-zA-Zñáéíóú\s]+" title="Solo letras" required>
							<input type="hidden" name="hidformcontrol" value="1">
						</div>

						<label for="blah">Imagen de la Promoción</label>
						<br>
						<place> Esta imagen sera utilizada para la visualizacion de la promoción</place>
						<div class="form-group col-12 text-center" style="border: 5px dashed #AED7FF">
							<img id="blah" width="500" height="250">
							<input id="inputimagen" style="width: 100%" type="file" name="ima" accept="image/*" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" ondragover='changeColor(event)'>
						</div>
						<!--div class="dropzone dropzone-previews col-8 text-center col-md-auto" style="height: 200px" id="myDropzone"></div-->
						<br>
						<div class="form-group col-12">
							<label for="descripcion">Descripción </label>
							<textarea type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripción detallada de la promoción." pattern="[a-zA-Zñáéíóú\s]+" title="Solo letras" cols="100" rows="10"></textarea>
						</div>
						<div class="form-group col-12">
							<label for="disclaimer">Disclaimer </label>
							<input type="text" class="form-control" id="disclaimer" name="disclaimer" placeholder="Limitaciones, restricciones o alcance de la promoción" pattern="[0-9a-zA-Zñáéíóú\s]+">
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label for="fechainicio">Fecha de Inicio</label>
								<input type="text" class="form-control" id="datepickerInicio" name="fechainicio" placeholder="Fecha de Inicio de la Promoción" required>
							</div>
							<div class="form-group col-6">
								<label for="fechafin">Fecha Final </label>
								<input type="text" class="form-control" id="datepickerFin" name="fechafin" placeholder="Fecha Final de la Promoción">
							</div>
						</div>
							<div class="form-group col-6">
								<label for="selmodal"> Desea que la promocion aparezca en el Pop-up inicial </label>
								<select class="custom-select lg-2 mb-2 mr-sm-2 mb-sm-0 form-control" id="selmodal" name="selmodal" required>
									<option> Si </option>
									<option> No </option>
								</select>
							</div>
							<div class="col-12"> 
								<button type="submit" class="btn btn-nv"> Ingresar Promoción </button>
							</div>
					</form>
					
					</div>
					<div class="col-12 text-right">
						<a href='./cargaPromociones.php' class='btn btn-danger btn-sm' role='button' aria-disabled='true'>Regresar <i class='fa fa-reply' aria-hidden='true'></i></a>
					</div>
				</div>
			</div>
			<br>
		</section>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/dropzone.js"></script>
		<script>

		function changeColor(event){
			document.getElementById('inputimagen').style.backgroundColor = "rgb(178, 247, 146)";
		}
		$( function() {
			$("#datepickerInicio").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: "dd-mm-yy"
			});
		});
		$( function() {
			$("#datepickerFin").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: "dd-mm-yy"
			});
		});
			$ ( function(){
			Dropzone.autoDiscover = false;
			Dropzone.options.myDropzone = {
				url: "yourUrl",
    			thumbnailWidth: null,
    			thumbnailHeight: null,
			dictDefaultMessage: "Haga click o arrastre para subir la imagen de la promocion",
    			maxFiles: 1,
   				init: function() {
        			this.on("thumbnail", function(file, dataUrl) {
            			$('.dz-image').last().find('img').attr({width: '', height: '220px'});
        			})

        			var prevFile;
            		this.on('addedfile', function() {
                	if (typeof prevFile !== "undefined") {
                    	this.removeFile(prevFile);
                	}
            		})

            		this.on('success', function(file, responseText) {
                		prevFile = file;
            		})
        		},

        			//this.on("success", function(file) {
            		//	$('.dz-image').css({"width":"100%", "height":"300px"});
        			//})
			};

			var myDropzone = new Dropzone('div#myDropzone');
			});
		</script>
	</body>
</html>
