<?php 
require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Promociones';
$_SESSION['ubix'] = 4;
if ($_SESSION['permisos']==1){
	$lugar = "Location://".ROOT_PATH_PHP;
	header($lugar);
}

?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catalogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<style>
		place{
			color:red;
		}
		</style>
	</head>
	<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');?>
		
		<section class="supertop">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-sm-10">
						<h3>Promociones</h3>
					</div>
					<div class="col-12 col-sm-2">
						<a href='./addPromo.php' class='btn btn-nv' role='button' aria-disabled='true'>Nueva Promoción <i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
					</div>
					<div class="col-12">
						<br>
						<place>Recuerde modificar las fechas de las promociones. Caso contrario, volverán a desactivarse automáticamente</place>
					</div>
					<div class="col-12 master1a">
						<table class="table table-striped table-bordered table-hover text-center">
							<thead>
								<tr>
									<th>#</th>
									<th>Nombre</th>
									<th>Inicio</th>	
									<th>Finalización</th>
									<th>Descripción</th>
									<th>Pop-up inicial</th>
									<th>Estado</th>
									<th colspan="2">Acciones</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$query = "Select id, titulo, descripcion, DATE_FORMAT(inicio, '%d-%m-%Y') as inicio, DATE_FORMAT(fin, '%d-%m-%Y') as fin, modal, estado from promociones order by estado";
									$sql = $conn->query($query);
									while($row = $sql->fetch_assoc()){
														
										if($row['estado']=='inactivo'){
											$estado = 'Promoción Finalizada';
											echo "<tr>
											<td>".$row['id']."</td>
											<td>".$row['titulo']."</td>
											<td>".$row['inicio']."</td>
											<td>".$row['fin']."</td>
											<td>".$row['descripcion']."</td>
											<td>".$row['modal']."</td>
											<td>".$estado."</td>
											<td colspan='2'><a href='./reaPromo.php?id=".$row['id']."' onclick=\"return confirm('Esta seguro que desea reactivar esta promoción ?')\" class='btn btn-success btn-sm' role='button' aria-disabled='true'>Reactivar Promoción <i class='fa fa-reply' aria-hidden='true'></i></a></td>
											</tr>
										";
										}else{
											$estado = 'Promoción Activa';
											echo "<tr>
											<td>".$row['id']."</td>
											<td>".$row['titulo']."</td>
											<td>".$row['inicio']."</td>
											<td>".$row['fin']."</td>
											<td>".$row['descripcion']."</td>
											<td>".$row['modal']."</td>
											<td>".$estado."</td>
											<td><a href='./modPromo.php?id=".$row['id']."' class='btn btn-nv btn-sm' role='button' aria-disabled='true'>Modificar <i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></td>
											<td><a href='./borPromo.php?id=".$row['id']."' class='btn btn-danger btn-sm' role='button' aria-disabled='true'>Desactivar <i class='fa fa-ban' aria-hidden='true'></i></a></td>
											</tr>
										";
										}
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<br>
		</section>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
	</body>
</html>
<?php 
$sqlpromociones = "Select * from promociones";
$sqlresul = $conn->query($sqlpromociones);
while($re = $sqlresul->fetch_assoc()){
	$hoy = date('Y-m-d', time()); 
	$prominicio = date('Y-m-d', strtotime($re['inicio']));
	$promfin = date('Y-m-d', strtotime($re['fin']));
	$idpromocion = $re['id'];
	if (($hoy >= $prominicio) && ($hoy <= $promfin)){
		$val = 1;
	}else{
		$updateprom = "Update promociones set estado = 'inactivo' where id = ?";	
		$sqlpr = $conn->prepare($updateprom);
		$sqlpr->bind_param('i', $idpromocion);
		$sqlpr->execute();

	}
}
?>