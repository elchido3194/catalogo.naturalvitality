<?php 
require_once('../../function/global.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 3;
$_SESSION['ubicacion'] = 'Misceláneos';
$_SESSION['ubix'] = 4;
if ($_SESSION['permisos']==1){
	$lugar = "Location://".ROOT_PATH_PHP;
	header($lugar);
}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catalogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/dropzone.css">
	<style>
	</style>
	
    </head>
	<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');
		?>
		<?php
			if (isset($_POST['tipodocumento'])){
				$tipodocumento = $_POST['tipodocumento'];
				$sql = "Insert into tipodocumentos(tipo) values (?);";
				$resul = $conn->prepare($sql);
				$resul->bind_param('s', $tipodocumento);
				if ($resul->execute()){
					echo "<script> alert('Tipo de Documento ingresado correctamente');</script>";
				}else{
					echo "<script> alert('No se pudo ingresar el tipo de documento. Por favor revise el tipo ingresado e intentelo nuevamente');</script>";
				}
			}
		?>
	<section class="supertop">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12 col-sm-10 col-md-8 text-center">
                	<br>
					<h1> Tipos de Documentos </h1>
					<hr>
					<h7>Para ingresar nuevos tipos de documentos, por favor hágalo a continuación </h7>
					<br> 
					<br> 
					<form method="POST">
					<div class="form-group">
						<label for='tipodocumento'>Tipo de Documento </label>
						<input type="text" class="form-control" name="tipodocumento" placeholder="Ingrese el nombre del tipo de documento a ser añadido"></input>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-nv">Añadir Tipo de Documento <i class="fa fa-plus" aria-hidden="true"></i></button>
					</div>
					</form>
					<hr>	
					<?php
					$sql = "SELECT * FROM tipodocumentos";
					$resul = $conn->query($sql);	
					echo "<table class='table table-striped table-bordered table-hover col-12'>
					<thead>
						<tr class='text-center'>
							<th> Tipos de Documentos presentes Actualmente</th>
							<th> Acciones </th> 
						</tr>
					</thead>
						<tbody>";
					while($row = $resul->fetch_assoc()){
						echo "<tr>
						<td>".$row['tipo']."</td><td><a class='btn btn-danger btn-sm' href='./eliminarTipoDoc.php?id=".$row['id']."' onclick=\"return confirm ('Esta seguro que desea eliminar este tipo de documento?')\" role='button' aria-disabled='true'>Borrar <i class='fa fa-ban' aria-hidden='true'></i></a></td>
						</tr>";
					}

					echo "</tbody></table>";
					?>
					<hr>
				</div>
			</div>
		</div>
	</section>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/dropzone.js"></script>
		<script>
		$(function(){
			Dropzone.autoDiscover = false;
			var myDropzone = new Dropzone('#midropzone', {
				acceptedFiles: ".pdf",
				url: "upload.php"
			});
			myDropzone.on("success", function(file, response){
				if (response != ""){
					var target = "data/" + response;
					document.getElementById("modalnombredocumento").value = response;
					document.getElementById("btn-smt-modal").disabled = false;
				}
			});

		});
		$("#btn-mod").click(function(){
			var sel = document.getElementById("myselect");
			if (sel.selectedIndex == -1 ){
				alert("Por favor seleccione un archivo");
			}else{ 
				document.getElementById("modalnombredocumento").value = document.getElementById("myselect").value;
				document.getElementById("modaltipodocumento").value = document.getElementById("txttipodocumento").value;
				document.getElementById("modalnuevodocumento").value = document.getElementById("myselect").value;
				$("#myModal").modal('show');
			}
			
		});
       
		$('#txttipodocumento').change(function() {
			document.getElementById("myselect").innerHTML = "";
    		var id = $(this).val(); 
    		$.ajax({
        		type:'POST',
        		url:'upload.php',
        		data:{'id':id},
				dataType: 'json',
        		success:function(json){
					$.each(json, function(i, value) {
						$('#myselect').append($('<option>').text(value).attr('value', value));
					});

      			}
    		});
		});

		$('#myselect').change(function() {
			var id = $(this).val();
    		$.ajax({
        		type:'POST',
        		url:'upload.php',
        		data:{'docname':id},
        		success:function(data){
					if (data != 1){
						var target = "data/" + data;
						document.getElementById("documento").style.display = "block";
						document.getElementById("documento").setAttribute('data', target)
					}else{
						alert("Error")
					}

      			}
    		});
		});


        $(function() {
        $("#tags").autocomplete({
            source: function( request, response ) {
				var term = request.term;
				$.getJSON( "users.php", request, function( data, status, xhr ) {
					var filtered = data.filter(function(carro){
						return carro.label.indexOf(term) !== -1;
					})
					response( filtered );
					});
				},
            minLength: 2,
			select: function (event, ui){
				$("#tags").val(ui.item.label);
       			$("#formbusqueda").submit();
			}
        	});
    	});
    	</script>
	</body>
</html>
