<?php 
require_once('../../function/global.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 3;
$_SESSION['ubicacion'] = 'Misceláneos';
$_SESSION['ubix'] = 4;
if ($_SESSION['permisos']==1){
	$lugar = "Location://".ROOT_PATH_PHP;
	header($lugar);
}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catalogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/dropzone.css">
    </head>
	<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');
			$displayform = "style='display:none'";
		?>
		<div id="myModal" class="modal fade">
    		<div class="modal-dialog modal-lg">
        		<div class="modal-content text-center">
            		<div class="modal-header">
                		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                		<h4 class="modal-title">Modificación de Documentos</h4>
            		</div>
            		<div class="modal-body">
                	<form action="" class="dropzone" id="midropzone" style="border: none;">
                    	<div class="row">
							<div class="form-group col-6">
								<label for="modalnombredocumento"> Título del Documento </label>
								<input type="text" class="form-control" id="modalnombredocumento" name="modalnombredocumento" placeholder="Nombre" required>
								<input type="hidden" id="modalnuevodocumento" name="modalnuevodocumento">
								<input type="hidden" id="documentid" name="documentid">
								<input type="hidden" id="documenttruename" name="documenttruename">
							</div>
							<div class="form-group col-6">
								<label for="modaltipodocumento"> Tipo de Documento </label>
								<select class="custom-select mb-2 mr-sm-2 mb-sm-0 form-control" id="modaltipodocumento" name="modaltipodocumento">
									<?php
										$query = "Select tipo from tipodocumentos;";
										$result = $conn->query($query);
										$fila = [];
										while($varia = $result->fetch_assoc()){
											$fila[] = $varia;	
										}
										foreach ($fila as $pu) {
											echo "<option>".$pu['tipo']."</option>\n";
										}
									?>
								</select>
							</div>
						</div>
						<div style="border: 5px double rgba(0, 0, 0, 0.3);" class="dz-message" data-dz-message><span>Haz click o arrastra para subir el nuevo archivo</span></div>
						<div class="form-group">
							<button onclick="return confirm('Esta seguro que desea cambiar este documento?');" id='btn-smt-modal' name='btn-smt-modal' type="submit" class="btn btn-nv" disabled>Registrar <i class="fa fa-pencil" aria-hidden="true"></i></button>
							<a href="../" class="btn btn-danger">Cancelar  <i class="fa fa-window-close" aria-hidden="true"></i></a>
						</div>
					</form>
                    </div>
        		</div>
    		</div>
		</div>
		<section class="supertop">
			<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12 col-sm-10 col-md-8 text-center">
                    <h4 class="text-center">Documentos</h4>
                    <h6 class="text-center"> </h6>
                    <?php
						if (isset($_GET['btn-eliminar'])) {
							if (isset($_GET['myselect'])){
							$titulodoc = $_GET['myselect'];
							$tipodoc = $_GET['txttipodocumento'];
							$query = "Select nombre from documentos where titulo = '".$titulodoc."' and estado = 'activo'";
							$result = $conn->query($query);
							$fila = $result->fetch_assoc();
							$nombredoc = $fila['nombre'];

							$sql = $conn->prepare("Update documentos set estado = 'inactivo' where nombre = ?");
							$sql->bind_param("s", $nombredoc);
							if ($sql->execute()){
								echo "<script>
								alert('Archivo Borrado Exitosamente');
								window.location.href='//".ROOT_PATH_PHP."miscelaneos'
								</script>";
							}
						}else{
							echo "<script>
							alert('Por favor seleccione un archivo');
							</script>";
						}

						}
						if(isset($_GET['btn-smt-modal'])){

							$nombredocumento = $_GET['modalnuevodocumento'];
							$titulodocumento = $_GET['modalnombredocumento'];
							$tipodocumento = $_GET['modaltipodocumento'];
							$iddocumento = $_GET['documentid'];
							$tituloarchivo = $_GET['documenttruename'];
							$sql = $conn->prepare("Update documentos set titulo = ?, tipo = ? where id = ?");
							$sql->bind_param("sss", $titulodocumento, $tipodocumento, $iddocumento);
							
							if($sql->execute()){
								// Cambiar el nombre del archivo anterior por temporal
								$oldname = 'data/'.$tituloarchivo;
								$newname = "data/temporal";
								echo $nombredocumento.'\n';//Catalogo de incentivos.pdf
								echo $titulodocumento.'\n';//test.pdf
								echo $iddocumento.'\n';//39.pdf
								echo $tituloarchivo.'\n';//Catalogo de Productos_9.pdf
								rename($oldname, $newname);
								// Cambiar el nombre del nuevo archivo por el archivo anterior
								$aoldname = "data/".$titulodocumento;
								rename($aoldname, $oldname);
								unlink($newname);
							echo "<script>
							alert('El documento ha sido modificado');
							</script>";
							}
						}				
					?>
                    <form action="" onsubmit="return confirm('Esta seguro que desea eliminar este documento?');">
                    	<div class="row">
							<div class="form-group col-6">
								<label for="myselect"> Título del Documento </label>
								<select class="custom-select mb-2 mr-sm-2 mb-sm-0 form-control" id="myselect" name="myselect">
								<option></option>
									<?php
										$query = "Select nombre, titulo from documentos where estado = 'activo';";
										$result = $conn->query($query);
										$fila = [];

										while($varia = $result->fetch_assoc()){
											$fila[] = $varia;
										}

										foreach ($fila as $pu) {
											echo "<option>".$pu['titulo']."</option>\n";
										}
									?>

								</select>
								<input type="hidden" id="formnuevodocumento" name="formnuevodocumento" value="1">
							</div>
							<div class="form-group col-6">
								<label for="txttipodocumento"> Tipo de Documento </label>
								<select class="custom-select mb-2 mr-sm-2 mb-sm-0 form-control" id="txttipodocumento" name="txttipodocumento">
									<option></option>
									<?php
										$query = "Select tipo from tipodocumentos;";
										$result = $conn->query($query);
										$fila = [];
										while($varia = $result->fetch_assoc()){
											$fila[] = $varia;
										}

										foreach ($fila as $pu) {
											echo "<option>".$pu['tipo']."</option>\n";
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<button type="button" id="btn-mod" class="btn btn-nv">Modificar <i class="fa file-pdf-o" aria-hidden="true"></i></button>
							<button type="submit" name="btn-eliminar" class="btn btn-warning"> Eliminar <i class="fa fa-trash" aria-hidden="true"></i></button>
							<a href="../" class="btn btn-danger">Cancelar  <i class="fa fa-window-close" aria-hidden="true"></i></a>
						</div>
					</form>
					<br>
					<object id="documento" style="display:none; width:100%; height:600px;" type="application/pdf">
					</object>
					<hr>
				</div>
			</div>
		</div>
		</section>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/dropzone.js"></script>
		<script>
		$(function(){
			Dropzone.autoDiscover = false;
			var myDropzone = new Dropzone('#midropzone', {
				acceptedFiles: ".pdf",
				url: "upload.php"
			});
			myDropzone.on("success", function(file, response){
				if (response != ""){
					var target = "data/" + response;
					document.getElementById("modalnombredocumento").value = response;
					document.getElementById("btn-smt-modal").disabled = false;
				}
			});

		});
		$("#btn-mod").click(function(){
			var sel = document.getElementById("myselect");
			if (sel.selectedIndex == -1 ){
				alert("Por favor seleccione un archivo");
			}else{ 
				document.getElementById("modalnombredocumento").value = document.getElementById("myselect").value;
				document.getElementById("modaltipodocumento").value = document.getElementById("txttipodocumento").value;
				document.getElementById("modalnuevodocumento").value = document.getElementById("myselect").value;
				$("#myModal").modal('show');
			}
			
		});
       
		$('#txttipodocumento').change(function() {
			document.getElementById("myselect").innerHTML = "";
    		id = $(this).val(); 
    		$.ajax({
        		type:'POST',
        		url:'upload.php',
        		data:{'id':id},
				dataType: 'json',
        		success:function(json){
					$.each(json, function(i, value) {
						$('#myselect').append($('<option>').text(value).attr('value', value));
					});

      			}
    		});
		});
		var option;
		var id;
		$('#myselect').change(function() {
			document.getElementById("txttipodocumento").innerHTML = "";
			id = $(this).val();
    		$.ajax({
        		type:'POST',
        		url:'upload.php',
        		data:{'docname':id},
        		success:function(data){
					if (data != 1){
						var value = JSON.parse(data);
						var target = "data/" + value.nombre;
						document.getElementById("documento").style.display = "block";
						document.getElementById("documento").setAttribute('data', target);
						document.getElementById("documentid").value = value.id;
						document.getElementById("documenttruename").value = value.nombre;
					}else{
						alert("Por favor seleccione un archivo");
					}

      			}
    		});
			$.ajax({
        		type:'POST',
        		url:'upload.php',
        		data:{'id':id},
				dataType: 'json',
        		success:function(json){
					$.each(json, function(i, value) {
						option = document.createElement('option');
						option.value = option.text = value.tipo;
						var tipodocumento = document.querySelector('#txttipodocumento');
						tipodocumento.add(option);
						//$('#txttipodocumento').append($('<option>').text(value).attr('value', value));
					});

      			}
    		});
		});


        $(function() {
        $("#tags").autocomplete({
            source: function( request, response ) {
				var term = request.term;
				$.getJSON( "users.php", request, function( data, status, xhr ) {
					var filtered = data.filter(function(carro){
						return carro.label.indexOf(term) !== -1;
					})
					response( filtered );
					});
				},
            minLength: 2,
			select: function (event, ui){
				$("#tags").val(ui.item.label);
       			$("#formbusqueda").submit();
			}
        	});
    	});
    	</script>
	</body>
</html>
