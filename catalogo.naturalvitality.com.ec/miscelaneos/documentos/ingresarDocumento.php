<?php 
require_once('../../function/global.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 3;
$_SESSION['ubicacion'] = 'Misceláneos';
$_SESSION['ubix'] = 4;
if ($_SESSION['permisos']==1){
	$lugar = "Location://".ROOT_PATH_PHP;
	header($lugar);
}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catalogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/dropzone.css">
    </head>
	<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');
			$displayform = "style='display:none'";
		?>
		
		<section class="supertop">
			<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12 col-sm-10 col-md-8 text-center">
                    <h4 class="text-center">Documentos</h4>
                    <h6 class="text-center"> </h6>
                    <?php
						if (isset($_GET['formnuevodocumento'])) {
							$nombredocumento = $_GET['formnuevodocumento'];
							$documentusuario = $_GET['txtnombredocumento'];
							$tipodocumento = $_GET['txttipodocumento'];
							$ext = pathinfo($documentusuario, PATHINFO_EXTENSION);
							
							try{
								$archiv ='./data/'.$nombredocumento;
								if ( !file_exists($archiv) ) {
									throw new Exception('No se pudo ingresar el archivo');
								}
								//sleep(1);
								$oldname = 'data/'.$nombredocumento;
								$newname = 'data/'.$documentusuario;
								$re = rename($oldname, $newname);
								if (!$re){
									throw new Exception('No se pudo ingresar el archivo.');
								}

								$sql = $conn->prepare("Insert into documentos(nombre, tipo, titulo, estado) values (?,?,?,?)");
								$numer = $conn->query("Select COUNT(tipo) as count from documentos where tipo ='".$tipodocumento."';");
								$numero = $numer->fetch_assoc();
								$titulo =  $tipodocumento."_".$numero['count'].".".$ext;
								$nombreuniq = $documentusuario;
								$sql->bind_param("ssss", $titulo, $tipodocumento, $nombreuniq, $activo);
								$activo = 'activo';
								$oldname = 'data/'.$documentusuario;
								$newname = 'data/'.$titulo;
								//sleep(1);
								$re = rename($oldname, $newname);
								if (!$re){
									throw new Exception('No se pudo ingresar el archivo.');
								}
								$sql->execute();
								echo "<script>
								alert('Documento Ingresado Exitosamente');
								</script>";
							}catch(Exception $e){
								
								echo "Error en la subida de los archivos. Verifique la información e intentelo de nuevo";
							}
						}
					?>
                    <form action="" class="dropzone" id="midropzone" style="border: none;">
                    	<div class="row">
							<div class="form-group col-6">
								<label for="txtnombredocumento"> Nombre del Documento </label>
								<input type="text" class="form-control" id="txtnombredocumento" name="txtnombredocumento" placeholder="Nombre" required>
								<input type="hidden" id="formnuevodocumento" name="formnuevodocumento">
							</div>
							<div class="form-group col-6">
								<label for="txttipodocumento"> Tipo de Documento </label>
								<select class="custom-select mb-2 mr-sm-2 mb-sm-0 form-control" id="txttipodocumento" name="txttipodocumento">
									<?php
										$query = "Select tipo from tipodocumentos;";
										$result = $conn->query($query);
										$fila = [];
										
										while($varia = $result->fetch_assoc()){
											$fila[] = $varia;
										}
										print_r($fila);

										foreach ($fila as $pu) {
											echo "<option>".$pu['tipo']."</option>\n";
										}
									?>
								</select>
							</div>
						</div>
						<div style="border: 5px double rgba(0, 0, 0, 0.3);" class="dz-message" data-dz-message><span>Haz click o arrastra para subir archivos</span></div>
						<div class="form-group">
							<button id='btn-smt' type="submit" class="btn btn-nv" disabled>Registrar <i class="fa fa-pencil" aria-hidden="true"></i></button>
							<a href="../" class="btn btn-danger">Cancelar  <i class="fa fa-window-close" aria-hidden="true"></i></a>
						</div>
					</form>
					<br>
                    <object id="documento" style="display:none; width:100%; height:600px;" type="application/pdf">
					</object>
					<hr>
				</div>
			</div>
		</div>
		</section>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/dropzone.js"></script>
		<script>
		$(function(){
			Dropzone.autoDiscover = false;
			var myDropzone = new Dropzone('#midropzone', {
				acceptedFiles: ".pdf, .xlsx",
				url: "upload.php"
			});
			myDropzone.on("success", function(file, response){
				if (response != ""){
					var target = "data/" + response;
					document.getElementById("documento").style.display = "block";
					document.getElementById("documento").setAttribute('data', target);
					document.getElementById("txtnombredocumento").value = response;
					document.getElementById("formnuevodocumento").value = response;
					document.getElementById("btn-smt").disabled = false;
				}
			});

		});
        $(function() {
        $("#tags").autocomplete({
            source: function( request, response ) {
				var term = request.term;
				$.getJSON( "users.php", request, function( data, status, xhr ) {
					var filtered = data.filter(function(carro){
						return carro.label.indexOf(term) !== -1;
					})
					response( filtered );
					});
				},
            minLength: 2,
			select: function (event, ui){
				$("#tags").val(ui.item.label);
       			$("#formbusqueda").submit();
			}
        	});
    	});

    	$( function() {
    			$( "#datepickerIngreso" ).datepicker({
      				changeMonth: true,
      				changeYear: true,
      				dateFormat: 'yy-mm-dd'
    			});
  			} );
  			$( function() {
    			$( "#datepickerCumple" ).datepicker({
      				changeMonth: true,
      				changeYear: true,
      				dateFormat: 'yy-mm-dd'
    			});
  			} );
  			var password = document.getElementById("txtpasswd1");
  			var confirm_password = document.getElementById("txtpasswd2");

			function validatePassword(){
			  if(password.value != confirm_password.value) {
			    confirm_password.setCustomValidity("Las contraseñas no son iguales");
			  } else {
			    confirm_password.setCustomValidity('');
			  }
			}

			password.onchange = validatePassword;
			confirm_password.onkeyup = validatePassword;
    	</script>
	</body>
</html>
