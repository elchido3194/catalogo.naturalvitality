
<?php 
require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'/function/redirect.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Misceláneos';
$_SESSION['ubix'] = 4;

if ($_SESSION['permisos']==1){
	$lugar = "Location://".ROOT_PATH_PHP;
	header($lugar);
}
?>
<!DOCTYPE html>
<html lang="es">
	<head>

		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catálogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/simplePagination.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	</head>
	<body>
		<?php require_once(ROOT_PATH_HTML.'link/nav.php');?>
		<section class="supertop">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<h3>Lista de Accesos</h3>
					</div>
				</div>
			</div>
			<div class="col-12 master1a">
				<table id="myTable" class="table table-striped table-bordered table-hover text-center">
					<thead>
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Usuario</th>
							<th>Fecha de Acceso</th>
							<th>Hora de Acceso</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$query = "Select id, nombre, apellido, usuario, DATE_FORMAT(fecha, '%d/%m/%Y') as fecha, hora from conexiones";
							$sql = $conn->query($query);
							while($row = $sql->fetch_assoc()){
								echo "<tr class='paginate'>
									<td>".$row['id']."</td>
									<td>".$row['nombre']. " ".$row['apellido']."</td>
									<td>".$row['usuario']."</td>
									<td>".$row['fecha']."</td>
									<td>".$row['hora']."</td>
									</tr>
								";
							}
						?>
					</tbody>
				</table><div id="page-nav"></div>
		</div>
		<br>
		</section>

		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 		<script src="<?php echo $_SESSION['nivelcap'];?>js/jquery.simplePagination.js"></script>
        <script>
    	$(function() {
    		$("#tablausuarios").pagination({
        	cssStyle: 'light-theme'
    		});

    		// Grab whatever we need to paginate
   			var pageParts = $(".paginate");

		    // How many parts do we have?
		    var numPages = pageParts.length;
		    // How many parts do we want per page?
		    var perPage = 10;

		    // When the document loads we're on page 1
		    // So to start with... hide everything else
		    pageParts.slice(perPage).hide();
		    // Apply simplePagination to our placeholder
		    $("#page-nav").pagination({
		        items: numPages,
		        itemsOnPage: perPage,
		        cssStyle: "light-theme",
		        // We implement the actual pagination
		        //   in this next function. It runs on
		        //   the event that a user changes page
		        onPageClick: function(pageNum) {
		            // Which page parts do we show?
		            var start = perPage * (pageNum - 1);
		            var end = start + perPage;

		            // First hide all page parts
		            // Then show those just for our page
		            pageParts.hide()
		            .slice(start, end).show();
		       	}
    		});
		});
        </script>
	</body>
</html>

