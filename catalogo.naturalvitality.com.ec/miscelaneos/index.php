<?php 
require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'/function/redirect.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Misceláneos';
$_SESSION['ubix'] = 4;
foreach (glob("documentos/data/*.*") as $filename) {
	$file = basename($filename);
	$sql = "Select nombre from documentos where nombre = '".$file."';";
	$result = $conn->query($sql);
	$rows = $result->num_rows;
	if ($rows == 0){
		unlink($filename);
	}
	//$row = $result->fetch_assoc();
	//echo "$filename size " . filesize($filename) . "\n";
	//echo $file;
}

foreach (glob("imgs/*.*") as $filename) {
	$file = basename($filename);
	$sql = "Select imagen from promociones where imagen = '".$file."';";
	$result = $conn->query($sql);
	$rows = $result->num_rows;
	if ($rows == 0){
		unlink($filename);
	}
	//$row = $result->fetch_assoc();
	//echo "$filename size " . filesize($filename) . "\n";
	//echo $file;
}

if ($_SESSION['permisos']==1){
	$lugar = "Location://".ROOT_PATH_PHP;
	header($lugar);
}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catalogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">

	</head>
	<body>
		<?php require_once(ROOT_PATH_HTML.'link/nav.php');?>
		<section class="supertop">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<h3>Usuarios</h3>
					</div>
					<div class="col-12 col-md-6 col-lg-3 master1a">
						<a href="//<?php echo ROOT_PATH_PHP?>usuarios" class="btn btn-nv btn-lg btn-block" role="button" aria-disabled="true">Usuarios <i class="fa fa-users" aria-hidden="true"></i></a>
					</div>
					<div class="col-12 col-md-6 col-lg-3 master1a">
						<a href="//<?php echo ROOT_PATH_PHP?>usuarios/tiposUsu.php" class="btn btn-nv btn-lg btn-block" role="button" aria-disabled="true">Puestos de Usuario <i class="fa fa-user-circle" aria-hidden="true"></i></a>
					</div>
					<!--div class="col-12 col-md-6 col-lg-3 master1a">
						<a href="./cargaPromociones.php" class="btn btn-nv btn-lg btn-block" role="button" aria-disabled="true">Promociones <i class="fa fa-tasks" aria-hidden="true"></i></a>
					</div-->
					<!--div class="col-12 col-sm-6 col-md-3 master1a">
						<a href="./puntosverdes/" class="btn btn-nv btn-lg btn-block" role="button" aria-disabled="true">Carga Puntos <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
					</div-->
					<div class="col-12 col-md-6 col-lg-3 master1a">
						<a href="./cambiarContra.php" class="btn btn-nv btn-lg btn-block" role="button" aria-disabled="true">Cambiar Contraseña <i class="fa fa-refresh" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
			<hr style="width: 85%;">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<h3>Promociones</h3>
					</div>
					<div class="col-12 col-md-6 col-lg-3 master1a">
						<a href="./cargaPromociones.php" class="btn btn-nv btn-lg btn-block" role="button" aria-disabled="true">Promociones <i class="fa fa-tasks" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>

			<hr style="width: 85%;">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<h3>Documentos</h3>
					</div>
					<div class="col-12 col-md-6 col-lg-4 master1a">
						<a href="./documentos/ingresarDocumento.php" class="btn btn-nv btn-lg btn-block" role="button" aria-disabled="true">Ingresar nuevo documento <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
					</div>
					<div class="col-12 col-md-6 col-lg-4 master1a">
						<a href="./documentos/modificarDocumento.php" class="btn btn-nv btn-lg btn-block" role="button" aria-disabled="true">Modificar o Borrar documento <i class="fa fa-trash" aria-hidden="true"></i></a>
					</div>
					<div class="col-12 col-md-6 col-lg-4 master1a">
						<a href="./documentos/tiposDocu.php" class="btn btn-nv btn-lg btn-block" role="button" aria-disabled="true">Tipos de Documentos <i class="fa fa-file-text-o" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
			<hr style="width: 85%;">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<h3>Productos</h3>
					</div>
					<div class="col-12 col-md-6 col-lg-4 master1a">
						<a href="//<?php echo ROOT_PATH_PHP?>productos/ingresarProducto.php" class="btn btn-nv btn-lg btn-block" role="button" aria-disabled="true">Ingresar Productos <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
					</div>
					<div class="col-12 col-md-6 col-lg-4 master1a">
						<a href="//<?php echo ROOT_PATH_PHP?>productos/actualizarProductos.php" class="btn btn-nv btn-lg btn-block" role="button" aria-disabled="true">Actualizar Productos <i class="fa fa-refresh" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
			<hr style="width:85%;">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<h3>Accesos</h3>
					</div>
					<div class="col-12 col-md-6 col-lg-4 master1a">
						<a href="./listaconexiones.php" class="btn btn-nv btn-lg btn-block" role="button" aria-disabled="true">Lista de Accesos <i class="fa fa-plug" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
			<hr style="width:85%;">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<h3>Puntos</h3>
					</div>
					<div class="col-12 col-md-6 col-lg-4 master1a">
						<a href="./puntosverdes/" class="btn btn-nv btn-lg btn-block" role="button" aria-disabled="true">Carga Puntos Usuarios <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
					</div>
					<div class="col-12 col-md-6 col-lg-4 master1a">
						<a href="./puntosverdes/puntosprod.php" class="btn btn-nv btn-lg btn-block" role="button" aria-disabled="true">Carga Puntos Productos <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		<br>
		</section>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
	</body>
</html>

