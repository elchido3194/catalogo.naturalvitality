<?php 
require_once('../../function/global.php');
require_once(ROOT_PATH_HTML.'link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 3;
$_SESSION['ubicacion'] = 'Misceláneos';
$_SESSION['ubix'] = 4;
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'link/meta.php');?>
		<title>Catalogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/dropzone.css">
	</head>
	
	<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');
			$displayform = "style='display:none'";
		?>
		<section class="supertop">
			<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12 col-sm-10 col-md-8 text-center">
                    <h4 class="text-center">Carga Puntos Productos</h4>
                    <h6 class="text-center">Suba aqui el archivo Excel de los puntos </h6> 
                    <form action="uploadprod.php" class="dropzone" id="midropzone" enctype='multipart/form-data'>
                    	<div class="dz-message" data-dz-message><span>Haz click o arrastra para subir archivos</span></div>
                    </form>
					<br>
                    <textarea style="font-family:'Courier New', monospace; width:100%; color:red;" name="consola" id="consola" cols="100" rows="10" disabled></textarea>
				</div>
			</div>
		</div>
		</section>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/dropzone.js"></script>
		<script>
		$(function(){
			Dropzone.autoDiscover = false;
			var myDropzone = new Dropzone('#midropzone');
			myDropzone.on("success", function(file, response){
				document.getElementById("consola").value = response;
			});
		});
    	</script>
	</body>
</html>
