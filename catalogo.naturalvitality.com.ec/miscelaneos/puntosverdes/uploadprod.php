<?php
require_once('../../link/db.php');
if (isset($_FILES['file'])){

	$sqlerase = "Update puntosverdesproductos set puntos = 0";
	$erase = $conn->prepare($sqlerase);
	if ($erase->execute()){
		$move = __DIR__.'/uploads/'.$_FILES['file']['name'];
		move_uploaded_file($_FILES['file']['tmp_name'], $move);
		$archivo = fopen($move, 'r');
		$data = array();
		while( ($line = fgetcsv($archivo)) !== false) {
 		   	$data[] = $line;
		}
		foreach($data as $key=>$user){
			$sql = "Select referencia from productos where referencia = '".$user[0]."';";
			$result = $conn->query($sql);
			$rows = $result->num_rows;
			$row = $result->fetch_assoc();
			if($rows > 0){
				$prep = $conn->prepare("Update puntosverdesproductos set puntos = ? where referencia = ?");
				$prep->bind_param("si", $user[1], $user[0]);
				if ($prep->execute()){
					//echo "sus puntos verdes se han actualizado a: ".$user[1]."\n";
					echo "Producto: ".$user[0]." actualizado. Puntos :".$user[1]." \n";
					//echo $key.". Usuario: ".$user[0]." correcto, ";
				}
			}else{
				echo $key.". Error en la linea ".$key." ";
				echo "Producto: ".$user[0]." no definido. No se puede actualizar sus puntos \n";
			}
		}
	}
}	
?>
