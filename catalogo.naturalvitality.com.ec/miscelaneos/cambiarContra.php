<?php 
require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'/function/global.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Nuevas Promociones';
$_SESSION['ubix'] = 4;
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catalogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/dropzone.css">
        
	</head>
		<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');?>
		<?php
		if (isset($_POST['hidformcontrol'])){
			$passactual =  $_POST['passactual'];
			$password =  $_POST['txtpasswd1'];
			$query = "Select id, passwd from users where username = '".$_SESSION['username']."';";
			$sql = $conn->query($query);
			$rows = $sql->fetch_assoc();
			if ($passactual == $rows['passwd']){
				$que = "Update users set passwd = ? where id=".$rows['id'];
				$sq = $conn->prepare($que);
				$sq->bind_param("s", $password); 
				if ($sq->execute()){
					echo "<script> alert('Contraseña cambiada correctamente');
				    	  </script>";
				}else{
						echo "<script> alert('Hubo un problema, por favor intentelo nuevamente');
				    	  </script>";
				}
			}else{
				echo "<script> alert('No se pudo cambiar la contraseña, por favor revise la contraseña actual e intentelo de nuevo');
			    	  </script>";
			}
		}
		?>
		<section class="supertop">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<h3>Cambio de Contraseña</h3>
						<hr>
					<form method="POST" enctype="multipart/form-data">
						<div class="form-group col-sm-12 col-md-6">
							<label for="nombrepromo">Ingresar la contraseña actual </label>
							<input type="password" class="form-control" id="passactual" name="passactual" placeholder="Por favor ingrese su contraseña actual" required>
							<input type="hidden" name="hidformcontrol" value="1">
						</div>
						<br>
					<div class="row">
							<div class="form-group col-sm-12 col-md-6">
								<label for="fechainicio">Nueva Contaseña</label>
								<input type="password" class="form-control" id="txtpasswd1" placeholder="Por favor ingrese su nueva contraseña" name="txtpasswd1" required>
							</div>
							<div class="form-group col-sm-12 col-md-6">
								<label for="fechafin">Confirmar Contraseña</label>
								<input type="password" class="form-control" id="txtpasswd2" placeholder="Por favor vuelva a ingresar su nueva contraseña" name="txtpasswd2">
							</div>
						</div>
						<div class="col-12"> 
								<button type="submit" class="btn btn-nv"> Cambiar Contraseña </button>
							</div>
					</form>
					
					</div>
					<div class="col-12 text-right">
						<a href='./cargaPromociones.php' class='btn btn-danger btn-sm' role='button' aria-disabled='true'>Regresar <i class='fa fa-reply' aria-hidden='true'></i></a>
					</div>
				</div>
			</div>
			<br>
		</section>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/dropzone.js"></script>
		<script>

		function changeColor(event){
			document.getElementById('inputimagen').style.backgroundColor = "rgb(178, 247, 146)";
		}
		var password = document.getElementById("txtpasswd1");
  			var confirm_password = document.getElementById("txtpasswd2");

			function validatePassword(){
			  if(password.value != confirm_password.value) {
			    confirm_password.setCustomValidity("Las contraseñas no son iguales");
			  } else {
			    confirm_password.setCustomValidity('');
			  }
			}

			password.onchange = validatePassword;
			confirm_password.onkeyup = validatePassword;
 
	</script>
	</body>
</html>
