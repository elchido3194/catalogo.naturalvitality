<?php 
require_once('./function/global.php');
require_once('./link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 1;
$_SESSION['ubicacion'] = 'Dashboard';
$_SESSION['ubix'] = 1;
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once('./link/meta.php');?>
		<title>Catalogo NV </title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<style>
		a {
			color: red;
		}
		</style>
	</head>
	<body>
    <nav class="navbar fixed-top navbar-expand-md navbar-dark bg-nv">
    <a class="navbar-brand" href="<?php echo '//'.ROOT_PATH_PHP;?>"></a>
    </nav>
    <section class="supertop imgbackLogin">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12 col-sm-10 col-md-8 text-center">
                    <img class="imglogin" src="./img/logo/logo.png">
                   		<br>
                    	<br>
                    	<div class="col-md-6 mx-auto">
							<form method="post">
								<div class="form-group">
									<label for="txtusuario"> Username </label>
									<input type="text" class="form-control" id="txtusuario" name="txtusuario" placeholder="Ingrese su Username">
								</div>
								
								<div class="form-group">
									<label for="txtcontrasena"> Contraseña </label>
									<input type="password" class="form-control" id="txtcontrasena" name="txtcontrasena" placeholder="Ingrese su contraseña">
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-nv">Ingresar</button>
								</div>
							</form>
							<?php
							if(isset($_POST['txtusuario'])){
								$txtusuario = strtolower($_POST['txtusuario']);
								$txtcontrasena = $_POST['txtcontrasena'];
								$sql = "SELECT nombre, apellido, username, passwd, estadoUsuario, tipoUsuario from users WHERE username = '".$txtusuario."';";
								$result = $conn->query($sql);
								$rows = $result->num_rows;
								$row = $result->fetch_assoc();
								//$result = mysqli_query($conn,$sql);
								//$rows = mysqli_num_rows($result);
								//$row = mysqli_fetch_assoc($result);
								$merror = "<p>Usuario y/o Contraseña incorrecta</p>";
								if($rows > 0){
									if ($row['estadoUsuario'] === 'activo' ){
										$usuario = strtolower($row['username']);
										if($txtusuario === $usuario){
											if($txtcontrasena === $row['passwd']){
												$_SESSION['username'] = $txtusuario;
												$nombre = $row['nombre'];
												$apellido = $row['apellido'];
												$_SESSION['permisos'] = $row['tipoUsuario'];
												$insql = "Insert into conexiones (nombre, apellido, usuario, fecha, hora) values (?,?,?,CURRENT_DATE, CURRENT_TIME);";
												$insert = $conn->prepare($insql);
												$insert->bind_param("sss", $nombre, $apellido, $txtusuario);
												if ($insert->execute()){
													echo "<script> window.location.href='//".ROOT_PATH_PHP."'</script>";
													//header('Location: ./mainpag.php');
												}
											}else{
												echo "<div class='alert alert-danger' role='alert'>Contraseña ingresada es incorrecta </div>";
												echo "<div class='alert alert-danger' role='alert'><a href='//".ROOT_PATH_PHP."cambiarContrasena.php'>¿Olvido su contraseña?</a></div>";
											}
										}
									}else{
										echo "<div class='alert alert-danger' role='alert'> Usuario Incorrecto </div>";
									}
								}else{
									echo "<div class='alert alert-danger' role='alert'> Usuario Incorrecto </div>";
								}
							}
							?>
                    	</div>
               	</div>
           	</div>
        </div>
    </section>
	</body>
</html>
