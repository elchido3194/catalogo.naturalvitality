<?php
require_once('../function/global.php');
require_once('../link/db.php');
$connec = new mysqli(SERV_PROPIORM,USUA_PROPIORM,CONT_PROPIORM,BBDD_PROPIORM);
mysqli_set_charset($connec, "utf8");
if ($connec->connect_error) {
   die("Connection failed: " . $connec->connect_error);
}


$sqlprod1 = "SELECT pp.id_product as id, ppl.name as nombre, reference as referencia, pcl.name as categoria, price as preciobase, price*1.12 as preciofinal, psa.quantity as cantidad, ppl.description as descripcion, ppl.description_short  as descripcioncorta from ps_product as pp join ps_product_lang as ppl on pp.id_product = ppl.id_product join ps_stock_available as psa on psa.id_product = pp.id_product join ps_category_lang as pcl on pcl.id_category = pp.id_category_default join ps_tax_rules_group as ptrg on pp.id_tax_rules_group = ptrg.id_tax_rules_group where pp.id_tax_rules_group = 6 group by pp.id_product";
$sqlprod2 = "SELECT pp.id_product as id, ppl.name as nombre, reference as referencia, pcl.name as categoria, price as preciobase, price as preciofinal, psa.quantity as cantidad, ppl.description as descripcion, ppl.description_short  as descripcioncorta from ps_product as pp join ps_product_lang as ppl on pp.id_product = ppl.id_product join ps_stock_available as psa on psa.id_product = pp.id_product join ps_category_lang as pcl on pcl.id_category = pp.id_category_default join ps_tax_rules_group as ptrg on pp.id_tax_rules_group = ptrg.id_tax_rules_group where pp.id_tax_rules_group = 5 group by pp.id_product";
$sqltax = "SELECT pp.id_product, pt.id_tax, ptrg.id_tax_rules_group, name, rate from ps_tax_rules_group as ptrg join ps_tax_rule as ptr on ptrg.id_tax_rules_group = ptr.id_tax_rules_group join ps_tax as pt on pt.id_tax = ptr.id_tax join ps_product as pp on pp.id_tax_rules_group = ptrg.id_tax_rules_group where pt.active = 1";
$sqlcarac0 = "SELECT  id_product, position, `value` from ps_feature as pf left join ps_feature_lang as pfl on pf.id_feature = pfl.id_feature left join ps_feature_product as pfp on pf.id_feature = pfp.id_feature join ps_feature_value as pfv on pfv.id_feature = pf.id_feature join ps_feature_value_lang as pfvl on pfvl.id_feature_value = pfp.id_feature_value where pfp.id_feature_value = pfv.id_feature_value and position = 0";
$sqlcarac1 = "SELECT  id_product, position, `value` from ps_feature as pf left join ps_feature_lang as pfl on pf.id_feature = pfl.id_feature left join ps_feature_product as pfp on pf.id_feature = pfp.id_feature join ps_feature_value as pfv on pfv.id_feature = pf.id_feature join ps_feature_value_lang as pfvl on pfvl.id_feature_value = pfp.id_feature_value where pfp.id_feature_value = pfv.id_feature_value and position = 1";
$sqlcarac2 = "SELECT  id_product, position, `value` from ps_feature as pf left join ps_feature_lang as pfl on pf.id_feature = pfl.id_feature left join ps_feature_product as pfp on pf.id_feature = pfp.id_feature join ps_feature_value as pfv on pfv.id_feature = pf.id_feature join ps_feature_value_lang as pfvl on pfvl.id_feature_value = pfp.id_feature_value where pfp.id_feature_value = pfv.id_feature_value and position = 2";

$ressqlprod1 = $connec->query($sqlprod1);
$ressqlprod2 = $connec->query($sqlprod2);
$ressqltax = $connec->query($sqltax);
$ressqlcarac0 = $connec->query($sqlcarac0);
$ressqlcarac1 = $connec->query($sqlcarac1);
$ressqlcarac2 = $connec->query($sqlcarac2);

while($rowsqlprod1 = $ressqlprod1->fetch_assoc()){
    $sqlconfirm = "SELECT * FROM productos WHERE id =".$rowsqlprod1['id'];
    $ressqlconfirm = $conn->query($sqlconfirm);
    $numrowsqlconfirm = $ressqlconfirm->num_rows;
    if($numrowsqlconfirm > 0){
        $sqlupdate = "UPDATE productos SET id = ?, nombre = ?, referencia = ?, categoria = ?, preciobase = ?, preciofinal = ?, cantidad = ?, estado = 1, descripcion = ?, descripcioncorta = ?, productolocal = 0 WHERE id =".$rowsqlprod1['id'];
        $ressqlupdate = $conn->prepare($sqlupdate);
        $ressqlupdate->bind_param("sssssssss", $rowsqlprod1['id'], $rowsqlprod1['nombre'], $rowsqlprod1['referencia'], $rowsqlprod1['categoria'], $rowsqlprod1['preciobase'], $rowsqlprod1['preciofinal'], $rowsqlprod1['cantidad'], $rowsqlprod1['descripcion'], $rowsqlprod1['descripcioncorta'] );
        if($ressqlupdate->execute()){
            echo "Producto: ".$rowsqlprod1['nombre']." actualizado correctamente. Referencia:".$rowsqlprod1['referencia']."\n";
        }else{
            echo "No se pudo actualizar el Producto: ".$rowsqlprod1['nombre']." Referencia:".$rowsqlprod1['referencia']."\n";
        }
    }else{
        $sqlupdate = "INSERT INTO productos(id, nombre, referencia, categoria, preciobase, preciofinal, cantidad, estado, descripcion, descripcioncorta, productolocal) values (?,?,?,?,?,?,?,1,?,?,0)";
        $ressqlupdate = $conn->prepare($sqlupdate);
        $ressqlupdate->bind_param("sssssssss", $rowsqlprod1['id'], $rowsqlprod1['nombre'], $rowsqlprod1['referencia'], $rowsqlprod1['categoria'], $rowsqlprod1['preciobase'], $rowsqlprod1['preciofinal'], $rowsqlprod1['cantidad'], $rowsqlprod1['descripcion'], $rowsqlprod1['descripcioncorta'] );
        if($ressqlupdate->execute()){
            echo "Producto: ".$rowsqlprod1['nombre']." actualizado correctamente. Referencia:".$rowsqlprod1['referencia']."\n";
        }else{
            echo "No se pudo actualizar el Producto: ".$rowsqlprod1['nombre']." Referencia:".$rowsqlprod1['referencia']."\n";
        }
    }
}
while($rowsqlprod2 = $ressqlprod2->fetch_assoc()){
    $sqlconfirm = "SELECT * FROM productos WHERE id =".$rowsqlprod2['id'];
    $ressqlconfirm = $conn->query($sqlconfirm);
    $numrowsqlconfirm = $ressqlconfirm->num_rows;
    if($numrowsqlconfirm > 0){
        $sqlupdate = "UPDATE productos SET id = ?, nombre = ?, referencia = ?, categoria = ?, preciobase = ?, preciofinal = ?, cantidad = ?, estado = 1, descripcion = ?, descripcioncorta = ?, productolocal = 0 WHERE id =".$rowsqlprod2['id'];
        $ressqlupdate = $conn->prepare($sqlupdate);
        $ressqlupdate->bind_param("sssssssss", $rowsqlprod2['id'], $rowsqlprod2['nombre'], $rowsqlprod2['referencia'], $rowsqlprod2['categoria'], $rowsqlprod2['preciobase'], $rowsqlprod2['preciofinal'], $rowsqlprod2['cantidad'], $rowsqlprod2['descripcion'], $rowsqlprod2['descripcioncorta'] );
        if($ressqlupdate->execute()){
            echo "Producto: ".$rowsqlprod2['nombre']." actualizado correctamente. Referencia:".$rowsqlprod2['referencia']."\n";
        }else{
            echo "No se pudo actualizar el Producto: ".$rowsqlprod2['nombre']." Referencia:".$rowsqlprod2['referencia']."\n";
        }
    }else{
        $sqlupdate = "INSERT INTO productos(id, nombre, referencia, categoria, preciobase, preciofinal, cantidad, estado, descripcion, descripcioncorta, productolocal) values (?,?,?,?,?,?,?,1,?,?,0)";
        $ressqlupdate = $conn->prepare($sqlupdate);
        $ressqlupdate->bind_param("sssssssss", $rowsqlprod2['id'], $rowsqlprod2['nombre'], $rowsqlprod2['referencia'], $rowsqlprod2['categoria'], $rowsqlprod2['preciobase'], $rowsqlprod2['preciofinal'], $rowsqlprod2['cantidad'], $rowsqlprod2['descripcion'], $rowsqlprod2['descripcioncorta'] );
        if($ressqlupdate->execute()){
            echo "Producto: ".$rowsqlprod2['nombre']." actualizado correctamente. Referencia:".$rowsqlprod2['referencia']."\n";
        }else{
            echo "No se pudo actualizar el Producto: ".$rowsqlprod2['nombre']." Referencia:".$rowsqlprod2['referencia']."\n";
        }
    }
}

while($rowsqltax = $ressqltax->fetch_assoc()){
    $sqlconfirm = "SELECT * FROM producttax WHERE id_product =".$rowsqltax['id_product'];
    $ressqlconfirm = $conn->query($sqlconfirm);
    $numrowsqlconfirm = $ressqlconfirm->num_rows;
    if ($numrowsqlconfirm > 0){
        $sqlupdate = "UPDATE producttax SET id_product = ?, id_tax = ?, name = ?, rate = ?  WHERE id_product =".$rowsqltax['id_product'];
        $ressqlupdate = $conn->prepare($sqlupdate);
        $ressqlupdate->bind_param("ssss", $rowsqltax['id_product'], $rowsqltax['id_tax'], $rowsqltax['name'], $rowsqltax['rate']);
        if($ressqlupdate->execute()){
            echo "Producto: ".$rowsqltax['id_product']." Impuesto actualizado \n";
        }else{
            echo "Producto: ".$rowsqltax['id_product']." Impuesto no se pudo actualizar \n";
        }
    }else{

        $sqlupdate = "INSERT into producttax(id_product, id_tax, name, rate) values(?,?,?,?)";
        $ressqlupdate = $conn->prepare($sqlupdate);
        $ressqlupdate->bind_param("ssss", $rowsqltax['id_product'], $rowsqltax['id_tax'], $rowsqltax['name'], $rowsqltax['rate']);
        if($ressqlupdate->execute()){
            echo "Producto: ".$rowsqltax['id_product']." Impuesto actualizado \n";
        }else{
            echo "Producto: ".$rowsqltax['id_product']." Impuesto no se pudo actualizar \n";
        }
    }
}
while($rowsqlcarac = $ressqlcarac0->fetch_assoc()){
    $sqlconfirm = "SELECT * FROM caracteristicas WHERE id_product =".$rowsqlcarac['id_product']." and position = 0";
    $ressqlconfirm = $conn->query($sqlconfirm);
    $numrowsqlconfirm = $ressqlconfirm->num_rows;
    if ($numrowsqlconfirm > 0){
        $sqlupdate = "UPDATE caracteristicas SET `value` = ? WHERE id_product =".$rowsqlcarac['id_product']." and position = 0";
        $ressqlupdate = $conn->prepare($sqlupdate);
        $ressqlupdate->bind_param("s", $rowsqlcarac['value']);
        if($ressqlupdate->execute()){
            echo "Producto: ".$rowsqlcarac['id_product']." Caracteristicas actualizadas \n";
        }else{
            echo "Producto: ".$rowsqlcarac['id_product']." No se pudo actualizar las caracteristicas:w \n";
        }
    }else{
        $sqlupdate = "INSERT into caracteristicas (id_product, position, `value`) values(?,0,?)";
        $ressqlupdate = $conn->prepare($sqlupdate);
        $ressqlupdate->bind_param("ss", $rowsqlcarac['id_product'], $rowsqlcarac['value']);
        if($ressqlupdate->execute()){
            echo "Producto: ".$rowsqlcarac['id_product']." Caracteristicas actualizadas \n";
        }else{
            echo "Producto: ".$rowsqlcarac['id_product']." No se pudo actualizar las caracteristicas:w \n";
        }

    }
}
while($rowsqlcarac = $ressqlcarac1->fetch_assoc()){
    $sqlconfirm = "SELECT * FROM caracteristicas WHERE id_product =".$rowsqlcarac['id_product']." and position = 1";
    $ressqlconfirm = $conn->query($sqlconfirm);
    $numrowsqlconfirm = $ressqlconfirm->num_rows;
    if ($numrowsqlconfirm > 0){
        $sqlupdate = "UPDATE caracteristicas SET `value` = ? WHERE id_product =".$rowsqlcarac['id_product']." and position = 1";
        $ressqlupdate = $conn->prepare($sqlupdate);
        $ressqlupdate->bind_param("s", $rowsqlcarac['value']);
        if($ressqlupdate->execute()){
            echo "Producto: ".$rowsqlcarac['id_product']." Caracteristicas actualizadas \n";
        }else{
            echo "Producto: ".$rowsqlcarac['id_product']." No se pudo actualizar las caracteristicas:w \n";
        }
    }else{
        $sqlupdate = "INSERT into caracteristicas (id_product, position, `value`) values(?,1,?)";
        $ressqlupdate = $conn->prepare($sqlupdate);
        $ressqlupdate->bind_param("ss", $rowsqlcarac['id_product'], $rowsqlcarac['value']);
        if($ressqlupdate->execute()){
            echo "Producto: ".$rowsqlcarac['id_product']." Caracteristicas actualizadas \n";
        }else{
            echo "Producto: ".$rowsqlcarac['id_product']." No se pudo actualizar las caracteristicas:w \n";
        }
    }
}
while($rowsqlcarac = $ressqlcarac2->fetch_assoc()){
    $sqlconfirm = "SELECT * FROM caracteristicas WHERE id_product =".$rowsqlcarac['id_product']." and position = 2";
    $ressqlconfirm = $conn->query($sqlconfirm);
    $numrowsqlconfirm = $ressqlconfirm->num_rows;
    if ($numrowsqlconfirm > 0){
        $sqlupdate = "UPDATE caracteristicas SET `value` = ? WHERE id_product =".$rowsqlcarac['id_product']." and position = 2";
        $ressqlupdate = $conn->prepare($sqlupdate);
        $ressqlupdate->bind_param("s", $rowsqlcarac['value']);
        if($ressqlupdate->execute()){
            echo "Producto: ".$rowsqlcarac['id_product']." Caracteristicas actualizadas \n";
        }else{
            echo "Producto: ".$rowsqlcarac['id_product']." No se pudo actualizar las caracteristicas:w \n";
        }
    }else{
        $sqlupdate = "INSERT into caracteristicas (id_product, position, `value`) values(?,2,?)";
        $ressqlupdate = $conn->prepare($sqlupdate);
        $ressqlupdate->bind_param("ss", $rowsqlcarac['id_product'], $rowsqlcarac['value']);
        if($ressqlupdate->execute()){
            echo "Producto: ".$rowsqlcarac['id_product']." Caracteristicas actualizadas \n";
        }else{
            echo "Producto: ".$rowsqlcarac['id_product']." No se pudo actualizar las caracteristicas:w \n";
        }
    }
}
?>