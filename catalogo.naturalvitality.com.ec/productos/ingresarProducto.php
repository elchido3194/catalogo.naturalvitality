<?php 
require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'/function/global.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Nuevos Productos';
$_SESSION['ubix'] = 2;
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catalogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/dropzone.css">
        <style type="text/css">
        	.row{
        		padding-right: 15px;
        		padding-left: 15px;
        	}
		place {
			color: grey;
		}
        </style>
        
	</head>
		<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');?>
		<?php
		
		if (isset($_POST['hidformcontrol'])){
			
			$nombreprod =  $_POST['nombreprod'];
			$referencia = $_POST['referencia'];
			$descripcion = $_POST['descripcion'];
			$descripcioncor = $_POST['descripcioncor'];
			$categoria = $_POST['txtcategoria'];
			$cantidad = $_POST['cantidad'];
			$preciobase = $_POST['preciobase'];
			$preciofinal = $_POST['preciofinal'];
			//$nombreImagen = $_POST['imagen'];
			$estado = 1;
			$productolocal = 1;
			$nombreImagen = $_FILES['ima']['name'];
			$ext = pathinfo($nombreImagen, PATHINFO_EXTENSION);
			$nombrepromo = "Producto_".$referencia;
			$imagen = $nombrepromo.'.'.$ext;
			$move = __DIR__.'/imgs/'.$imagen;
			
			$query = "Insert into productos (imagen, nombre, referencia, categoria, preciobase, preciofinal, cantidad, estado, descripcion, descripcioncorta, productolocal) values (?,?,?,?,?,?,?,?,?,?,?)";
			$querypuntos = "Insert into puntosverdesproductos(referencia, ultimaModificacion, creacionRegistro) values (?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
			$sql = $conn->prepare($query);
			$sqlpuntos = $conn->prepare($querypuntos);
			$sql->bind_param("sssssssisss", $imagen, $nombreprod, $referencia, $categoria, $preciobase, $preciofinal, $cantidad, $estado, $descripcion, $descripcioncor, $productolocal); 
			$sqlpuntos->bind_param("s", $referencia);
			move_uploaded_file($_FILES['ima']['tmp_name'], $move);
			
			if ($sql->execute()){
				$querytaxcon = "Select * from productos where referencia=".$referencia;
				$sqlquerytaxcon = $conn->query($querytaxcon);
				$resquerytaxcon = $sqlquerytaxcon->fetch_assoc();
				if ($preciobase == $preciofinal){
					$querytax = "INSERT INTO producttax(id_product, id_tax, name, rate) VALUES (?,2,'Excepto IVA',0.000)";
					$sqltax = $conn->prepare($querytax);
					$sqltax->bind_param('s', $resquerytaxcon['id']);
					if ($sqltax->execute()){
						if($sqlpuntos->execute()){
						echo "<script> alert('Ingreso Correcto');
						  </script>";
						}
					}
				}else{
					$querytax = "INSERT INTO producttax(id_product, id_tax, name, rate) VALUES (?,1,'IVA (14%)',14.000)";
					$sqltax = $conn->prepare($querytax);
					$sqltax->bind_param('s', $resquerytaxcon['id']);
					if ($sqltax->execute()){
						echo "<script> alert('Ingreso Correcto');
				    	  </script>";
					}
			
				}
				//$move = __DIR__.'/imgs/'.$imagen;
				//move_uploaded_file($_FILES['ima']['tmp_name'], $move);
				//$quer = "Update promociones set imagen = ?, nombre = ? where id = ".$idnum;
				//$sq = $conn->prepare($quer);
				//$sq ->bind_param("ss", $imagen, $nombrepromo);
				//if($sq->execute()){
							 // }
			}else{
				echo "<script> alert('No se pudo ingresar el registro. Por favor verifique los datos del producto e intentelo de nuevo');
				    	  </script>";
			}
		}
		?>
		<section class="supertop">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<h3>Nuevo Producto</h3>
						<hr>
					<form method="POST" enctype="multipart/form-data">
						<div class="row">
							<div class="form-group col-12 col-md-6">
								<label for="nombreprod">Nombre del Producto </label>
								<input type="text" class="form-control" id="nombreprod" name="nombreprod" placeholder="Ingrese el nombre del producto" pattern="[0-9a-zA-Záéíóú\s]+" title="Solo letras" required>
								<input type="hidden" name="hidformcontrol" value="1">
							</div>
							<div class="form-group col-12 col-md-6">
								<label for="referencia"># Referencia</label>
								<input type="text" class="form-control" id="referencia" name="referencia" placeholder="Ingrese el número de Referencia" required>
							</div>
						</div>
						<hr>
						<!--div class="form-group col-6">
							<label for="imagen">Imagen del Producto</label>
							<input type="text" class="form-control" id="imagen" name="imagen" placeholder="Ingrese la URL de la imagen del producto" required>
						</div-->
						<label for="ima">Imagen del Producto</label>
						<br>
						<place> Esta imagen será utilizada para la visualización del producto</place>
						<br>
						<place> Tamaño: 1200px x 1200px</place>
						<div class="form-group col-12 text-center" style="border: 5px dashed #AED7FF">
							<img id="blah" width="500" height="250">
							<input id="inputimagen" style="width: 100%" type="file" name="ima" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" ondragover='changeColor(event)'>
						</div>
						
						<br>
						<div class="form-group col-12">
							<label for="descripcioncor">Descripción Corta </label>
							<br>
							<place> Descripción breve acerca del producto </place>
							<textarea type="text" class="form-control" id="descripcioncor" name="descripcioncor" placeholder="Descripción breve acerca del producto " pattern="[a-zA-Záéíóú\s]+" title="Solo letras" cols="100" rows="3"></textarea>
						</div>
						<div class="form-group col-12">
							<label for="descripcion">Descripción Completa </label>
							<br>
							<place> Descripción detallada acerca del producto. Incluye indicaciones, contraindicaciones, advertencias, efectos secundarios, composición, etc </place>
							<textarea type="text" class="form-control" id="descripcion" name="descripcion" rows="10"></textarea>
						</div>
						<hr>
						
						<div class="row">
						<div class="form-group col-12 col-md-6">
							<label for="txtcategoria"> Categoría</label>
							<select class="custom-select mb-2 mr-sm-2 mb-sm-0 form-control" id="txtcategoria" name="txtcategoria" required>
									<?php
										//$query = "SELECT productos.categoria FROM productos GROUP BY categoria";
										$query = "SELECT categoria from categoriaproductos";
										$result = $conn->query($query);
										$fila = [];
										while($varia = $result->fetch_assoc()){
											$fila[] = $varia;
										}
										foreach ($fila as $pu) {
											echo "<option>".$pu['categoria']."</option>\n";
										}
									?>
								</select>
						</div>
						<div class="form-group col-12 col-md-6">
							<label for="cantidad">Cantidad</label>
							<input type="text" class="form-control" id="cantidad" name="cantidad" placeholder="Cantidad de productos disponibles">
						</div>
					</div>
					<div class="row">
						<div class="form-group col-12 col-md-6">
							<label for="preciobase">Precio Base</label>
							<input class="form-control" type="text" name="preciobase" id="preciobase" placeholder="Precio Base" pattern="[\d\.]+">
						</div>
						<div class="form-group col-12 col-md-6">
							<label for="preciofinal">Precio Final</label>
							<input class="form-control" type="text" name="preciofinal" id="preciofinal" placeholder="Precio Final" pattern="[\d\.]+">
							<place> Si no grava iva, el precio base y el final seran iguales</place>
						</div>
					</div>
					<hr>
							<div class="col-12"> 
								<button type="submit" class="btn btn-nv"> Ingresar Producto </button>
							</div>
					</form>
					
					</div>
					<div class="col-12 text-right">
						<a href='../' class='btn btn-danger btn-sm' role='button' aria-disabled='true'>Regresar <i class='fa fa-reply' aria-hidden='true'></i></a>
					</div>
				</div>
			</div>
			<br>
		</section>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/dropzone.js"></script>
		<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=6qo6cb7x89n9x8sr4rsx0nnqhtjc8ep1l6779g81jaf7taef"></script>
		<script>
		tinymce.init({
		selector: 'textarea',
		height: 230,
		plugins: 'link image code',
		relative_urls: false,
  		content_css: [
   			'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    		'//www.tinymce.com/css/codepen.min.css' ]
 		});

		function changeColor(event){
			document.getElementById('inputimagen').style.backgroundColor = "rgb(178, 247, 146)";
		}
		$( function() {
			$("#datepickerInicio").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: "yy-mm-dd"
			});
		});
		$( function() {
			$("#datepickerFin").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: "yy-mm-dd"
			});
		});
			$ ( function(){
			Dropzone.autoDiscover = false;
			Dropzone.options.myDropzone = {
				url: "yourUrl",
    			thumbnailWidth: null,
    			thumbnailHeight: null,
			dictDefaultMessage: "Haga click o arrastre para subir la imagen de la promocion",
    			maxFiles: 1,
   				init: function() {
        			this.on("thumbnail", function(file, dataUrl) {
            			$('.dz-image').last().find('img').attr({width: '', height: '220px'});
        			})

        			var prevFile;
            		this.on('addedfile', function() {
                	if (typeof prevFile !== "undefined") {
                    	this.removeFile(prevFile);
                	}
            		})

            		this.on('success', function(file, responseText) {
                		prevFile = file;
            		})
        		},

        			//this.on("success", function(file) {
            		//	$('.dz-image').css({"width":"100%", "height":"300px"});
        			//})
			};

			var myDropzone = new Dropzone('div#myDropzone');
			});
		</script>
	</body>
</html>
