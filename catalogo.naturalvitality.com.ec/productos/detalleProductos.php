<?php 
require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'/function/redirect.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Productos';
$_SESSION['ubix'] = 2;
$connec = new mysqli(SERV_PROPIORM,USUA_PROPIORM,CONT_PROPIORM,BBDD_PROPIORM);
mysqli_set_charset($connec, "utf8");
if ($connec->connect_error) {
   die("Connection failed: " . $connec->connect_error);
}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catalogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<style type="text/css">
			tagdes h4 {
				font-size: 100%;
				color: black !important;
			}
			taggrande{
				font-size: 125%;
			}
			.descuento {
				color: white;
				background: #f13340;
				border: 1px solid #d02a2c;
				margin-left: 100px;
				margin-right: 100px;
			}
		</style>
	</head>
	<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');?>
		<div class='divpaginacompleta'>
		<section class="buscadorInicial">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-12 col-sm-10 col-md-8">
						<h2>Productos</h2>
					</div>
				</div>
			</div>
			<br>
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-12 col-sm-10 col-md-8 text-center">
						<h4>Busqueda de Productos:</h4>
						<form method="GET" action="index.php">
							<div class="form-group">
								<input type="text" class="form-control" id="txtnombreprod" name="txtnombreprod" placeholder="Nombre del Producto">
								<input type="hidden" value="1" name="formController">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-nv">Buscar <i class="fa fa-search" aria-hidden="true"></i></button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<br>
			<br>
		</section>
			<section class="showproducto">
				<div class="row justify-content-center align-items-center">
					<?php
					if (isset($_GET['id'])){
						$id = $_GET['id'];
						$sql = "SELECT productos.id, imagen, nombre, productos.referencia, categoria, round(preciobase, 2) as preciobase, round(preciofinal, 2) as preciofinal, cantidad, estado, descripcion, descripcioncorta, productolocal, id_product, id_tax, name, rate, puntos FROM productos JOIN producttax ON producttax.id_product = productos.id join puntosverdesproductos as pvp on pvp.referencia = productos.referencia WHERE productos.id ='$id'";
						$sqlficha = "Select position, value from caracteristicas where id_product = $id";
						$sqldescuento = "SELECT id_product, round(reduction, 2) as reduction, reduction_type from ps_specific_price where now() >= `from` and now() <= `to`and id_product = $id";
						//$sqltax = "Select * from producttax where id_product = $id";
						$resul = $conn->query($sql);
						$resulficha = $conn->query($sqlficha);
						$resuldescuentos = $connec->query($sqldescuento);
						//$resultax = $conn->query($sqltax);
						//$taxval = $resultax->fetch_assoc();
						$rows = $resul->num_rows;
						$rowsdescuento = $resuldescuentos->num_rows; 
						$rowdes = $resuldescuentos->fetch_assoc();
						while ($row = $resul->fetch_assoc()) {
							if ($row['productolocal']==0){
								$sqlimagen = "SELECT id_image, ppl.id_product, link_rewrite
								FROM ps_image AS ps
								JOIN ps_product_lang AS ppl ON ps.id_product = ppl.id_product
								where ppl.id_product = ".$row['id']."
								GROUP BY link_rewrite";
								$resulimagen = $connec->query($sqlimagen);
								$rowima = $resulimagen->fetch_assoc();
								echo "<div class='col-4'><img src='http://tienda.naturalvitality.com.ec/".$rowima['id_image']."-thickbox_default/".$rowima['link_rewrite'].".jpg' class='img-fluid'></div>";
								echo "<div class='col-6 text-center'>
								<h1>".$row['nombre']."</h1><taggrande>
								<label> # Referencia: ".$row['referencia']."</label>
								<br>";
								if (!empty($row['puntos'])){
									echo "<button class='btn btn-success'>Puntos <span class='badge badge-light'>".$row['puntos']."</span></button>";
								}
								echo "<br><br>";
								if ($rowsdescuento > 0){
									$precioresta = $row['preciofinal'] - $row['preciobase'];
									if($precioresta == 0){
										$preciofinal = ((1-$rowdes['reduction'])* $row['preciobase']);
										$descuento = $rowdes['reduction']*100;
										echo "<h5> Precio Normal : $ ".$row['preciobase']."</h5>";
										echo "<h5 class ='descuento'> Descuento : ".$descuento." %</h5>";
										echo "<h2> PVP : <b>$ ".$preciofinal."</b></h2>";
									}else{
										$preciofinal = ((1-$rowdes['reduction'])* $row['preciobase']);
										$precioresta = $preciofinal * 0.12;
										$preciofinalfinal = $preciofinal + $precioresta;
										$descuento = $rowdes['reduction']*100;
										echo "<h5> Precio Normal : $ ".$row['preciobase']."</h5>";
										echo "<h5 class ='descuento'> Descuento : ".$descuento." %</h5>";
										echo "<h2> PVP : <b>$ ".round($preciofinalfinal, 2)."</b></h2>";
									}
								}else{
									$precioresta = $row['preciofinal'] - $row['preciobase'];
									echo "<h5> Precio : $ ".$row['preciobase']."</h5>";
									echo "<h5> Impuesto : $ ".$precioresta."</h5>";
									echo "<h2> PVP : <b>$ ".$row['preciofinal']."</b></h2>";
								}
								echo "<br> 
								<br>
									<tagdes>".$row['descripcioncorta']."<tagdes>
									<br>
								</taggrande>
								</div>
							 </div>
						</section>
						<br>
						<section>
						<div class='justify-content-center'>
							<h3 class='page-product-heading'> Ficha Técnica </h3>
							<br>
						</div>
						<table class='table-data-sheet'>
						<tbody>";
						
						while($ficha = $resulficha->fetch_assoc()){
							if ($ficha['position'] == 0){
								echo "<tr class='odd'><td>Presentación</td><td>".$ficha['value']."</td></tr>";
							}else if ($ficha['position'] == 1){
								echo "<tr class='odd'><td>Contenido</td><td>".$ficha['value']."</td></tr>";
							}else if ($ficha['position']==2){
								echo "<tr class='odd'><td>Contenido Neto</td><td>".$ficha['value']."</td></tr>";
							}
						}
						echo "
						</tbody>
						</table>
						<br>
						</section>
						<section class='mayorInformacion'>
							<div class='justify-content-center'>
								<h3 class='page-product-heading'> Más Información </h3>
							</div>
						<div class='col-1'>
						</div>
						<div class='col-8' style='margin: auto; word-wrap: break-word; padding: 0 20px 20px'>
						";
						$masinformacion = $row['descripcion'];
						$masinfo = str_replace('\n', ' ' , $masinformacion);
						echo $masinfo;
					}else{
						echo "<div class='col-4'><img src='imgs/".$row['imagen']."' class='img-fluid'></div>";
						echo "<div class='col-6 text-center'>
								<h1>".$row['nombre']."</h1>
								<label> # Referencia: ".$row['referencia']."</label>
								<br>";
								if (!empty($row['puntos'])){
									echo "<button class='btn btn-success'>Puntos <span class='badge badge-light'>".$row['puntos']."</span></button>";
								}
								echo "<br><br>";
								$precioresta = $row['preciofinal'] - $row['preciobase'];
								echo "<h5> Precio : $".$row['preciobase']."</h5>";
								echo "<h5> Impuesto : $".$precioresta."</h5>";
								echo "<h2> PVP : <b>$".$row['preciofinal']."</b></h2>";
								echo "<br>
								<br>
									<tagdes><h4>".$row['descripcioncorta']."</h4></tagdes>
									<br><br>
								</div>
							 </div>
						</section>
						<br>";
						echo "<section class='mayorInformacion'>
							<div class='justify-content-center'>
								<h3 class='page-product-heading'> Más Información </h3>
							</div>
						<div class='col-1'>
						</div>
						<div class='col-8' style='margin: auto; word-wrap: break-word; padding: 0 20px 20px'>
						<br>";
						$masinformacion = $row['descripcion'];
						$masinfo = str_replace('\n', ' ' , $masinformacion);
						echo "<tagdeslar>".$masinfo."</tagdeslar>";
					}
				}
			}
			?>
			</div>
			<div class="col-12 text-right">
				<a href='//<?php echo ROOT_PATH_PHP?>productos' class='btn btn-danger btn-sm' role='button' aria-disabled='true'>Regresar <i class='fa fa-reply' aria-hidden='true'></i></a>
			</div>
			</section>
			<br>
		</div>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
	</body>
</html>
