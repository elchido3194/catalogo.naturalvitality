<?php 
require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'/function/redirect.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Productos';
$_SESSION['ubix'] = 2;
$mostrarTabla = 'none';
$sqlprom = "SELECT categoria, icon FROM categoriaproductos GROUP BY categoria";
$resultprom = $conn->query($sqlprom);
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catálogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<style type="text/css">
			
			@font-face{
				font-family: "fontello";
				src: url("fontello/fontello.woff") format("woff"),
				url("fontello/fontello.woff2") format("woff2"),
				url("fontello/fontello.eot") format("embedded-opentype"),
				url("fontello/fontello.ttf") format("truetype"),
				url("fontello/fontello.svg") format("svg");
				font-style: normal;
			}
			
			[data-icon]:before {
				font-family: fontello;
				font-style: normal;
				font-size: 450%;
				content: attr(data-icon);
			}
			.carda:hover{
				color: #fff;
				background-color: #30309A;
			}
			.cardsection{
				padding-left: 10px;
				padding-right: 10px;
			}
			hr{
				width: 90%;
			}
			h5 {
				color: gray;
				font-family: sans-serif;
			}
			.showproducto {
				width: 90%;
			}
			.buscadorInicial{
				padding-top: 85px;
				min-height: 25vh;
			}
			.mayorInformacion{
				width: 100%;

			}
			table{
				max-width: 100%;
				border-collapse: collapse;
				border-spacing: 0;
			}

			table td, table th {
				vertical-align: middle;
				font-weight: normal;
				padding: 9px 10px;
				text-align: center;
			}

			h3.page-product-heading {
			    color: #fff;
			    text-align: center;
			    text-transform: uppercase;
			    font-family: "Titillium Web",sans-serif;
			    font-weight: 600;
			    font-size: 18px;
			    line-height: 20px;
			    padding: 14px 20px 17px;
			    margin: auto;
			    margin: 0 0 20px;
			    position: relative;
			    background: rgba(66,139,202,1);
			    width: 90%;
			}
			#componentex, #myTable{
	    		margin: 0 auto;
	    		font-family: "Dosis";
			}
			#myTable {
				margin: auto;
				width: 80%;
			}

			#componentex tr td {
	    		border-collapse: collapse;
	    		border: 1px solid rgba(66,139,202,1);
			}
			#componentex-1 {
	    		background-color: rgba(66,139,202,1);
	    		color: #fff;
	    		text-transform: uppercase;
			}
		</style>
	</head>
	<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');?>
		<section class="buscadorInicial">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-xs-12 col-sm-8 col-md-10">
					</div>
					<div class="col-xs-12 col-sm-4 col-md-2">
						<?php
							if ($_SESSION['permisos'] == 2){
								echo "<a href=ingresarProducto.php class='btn btn-nv'> Nuevo Producto </a>";
							}
						?>
					</div>
				</div>
			</div>
			<br>
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-12 col-sm-10 col-md-8 text-center">
						<h2>Búsqueda de Productos NV</h2>
						<br>
						<form method="GET">
							<div class="form-group">
								<input type="text" class="form-control" id="txtnombreprod" name="txtnombreprod" placeholder="Búsqueda de Productos" required>
								<input type="hidden" value="1" name="formController">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-nv1">Buscar <i class="fa fa-search" aria-hidden="true"></i></button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<br>
			<br>
		</section>
		<?php
			if (isset($_GET['formController'])){
				$mostrarTabla = 'table';
			}
		?>
		<div class="container-fluid">
			<table id="myTable" class="table table-striped table-bordered table-hover text-center" style="display: <?php echo $mostrarTabla;?>;">
				<thead>
					<tr>
						<th>Ref</th>
						<th>Producto</th>
						<th>Categoria</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if (isset($_GET['formController'])){
							$nombrebusqueda = $_GET['txtnombreprod'];
							$sql = "Select id, nombre, categoria, referencia, productolocal from productos where nombre LIKE '%$nombrebusqueda%' and estado = 1 union select id, nombre, categoria, referencia, productolocal from productos where nombre LIKE '%$nombrebusqueda' and estado = 1 union select id, nombre, categoria, referencia, productolocal from productos where nombre LIKE '$nombrebusqueda%' and estado = 1 union select id, nombre, categoria, referencia, productolocal from productos where referencia = '$nombrebusqueda' and estado = 1 union select id, nombre, categoria, referencia, productolocal from productos where descripcioncorta like '%$nombrebusqueda%' and estado = 1 union select id, nombre, categoria, referencia, productolocal from productos where categoria = '$nombrebusqueda' and estado = 1";
							$resul = $conn->query($sql);
							$rows = $resul->num_rows;
							if($rows > 0){
								while ($row = $resul->fetch_assoc()) {
									echo "<tr class='paginate'>
										<td>".$row['referencia']."
										<td><a class='link-nv' href='detalleProductos.php?id=".$row['id']."'><b>".$row['nombre']."</b></a></td>
										<td>".$row['categoria']."</td>";
									if (($_SESSION['permisos']== 2)&&($row['productolocal']== 1)){
										echo "<td><a href='./modificarProducto.php?id=".$row['id']."' class='btn btn-nv btn-sm' role='button' aria-disabled='true'>Modificar <i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></td>
											";
										echo "<td><a href='./eliminarProducto.php?id=".$row['id']."' onclick=\"return confirm ('Esta seguro que desea eliminar este producto')\" class='btn btn-danger btn-sm' role='button' aria-disabled='true'>Eliminar <i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></td>";
									}
									echo "</tr>";
								}
							}else{
								echo "<tr><td colspan='3'> <h3>No se encontro ningun resultado</h3> </td></tr>";
							}
						}
					?>
				</tbody>
			</table>
		</div>
		<br>
		<section class="cardsection">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 text-center">
						<h2>Navegación por Categorías</h2>
					</div>
					<hr>
					<?php 
							while ($prom = $resultprom->fetch_assoc()){
								echo'<div class="col-12 col-sm-6 col-md-4 master1a"><div class="card carda text-center" style="cursor: pointer" onclick="buscarCategoria(\''.$prom['categoria'].'\')">';
								//echo'<a data-lightbox="image-1" href="miscelaneos/imgs/'.$prom['imagen'].'"><img class="card-img-top" src="miscelaneos/imgs/'.$prom['imagen'].'" alt=""></a>';
								echo '<i aria-hidden="true" data-icon="'.$prom['icon'].'"></i>';
								echo'<div class="card-body">';
								echo '<h3>'.$prom['categoria'].'</h3>';
								//echo'<p class="card-text">'.$prom['descripcion'].'</p>';
								//echo'<button href="#" class="btn btn-nv1">Más Información <i class="fa fa-info-circle" aria-hidden="true"></i></button></div></div></div>';
								echo "</div></div></div>";
							}
						?>
				</div>
			</div>
		</section>
		
		<br>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script>
		function buscarCategoria(datos){
			window.location.href='//<?php echo ROOT_PATH_PHP;?>productos/?txtnombreprod='+datos+'&formController=1';
		}
		</script>
	</body>
</html>
