<?php
require_once('./function/global.php');
require_once('./function/redirect.php');
require_once('./link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 1;
$_SESSION['ubicacion'] = 'Dashboard';
$_SESSION['ubix'] = 1;

$sql = "SELECT nombre, apellido, correo, users.username, users.ultimaModificacion, puntos FROM users join puntosverdes on users.username = puntosverdes.username WHERE users.username = '".$_SESSION['username']."';";
$sqlprom = "SELECT id, nombre, titulo, descripcion, disclaimer, modal, imagen, inicio, fin FROM promociones WHERE estado = 'activo';";
$sqlmod = "SELECT id, nombre, titulo, descripcion, disclaimer, imagen, inicio, fin FROM promociones WHERE estado = 'activo' AND modal = 'si';";
$resultprom = $conn->query($sqlprom);
$result = $conn->query($sql);
$resmodal = $conn->query($sqlmod);
$rows = $result->num_rows;
$row = $result->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once('./link/meta.php');?>
		<title>Catálogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>

		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/lightbox.css">

		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script>
			$(function(){
				$("#accordion").accordion({
					collapsible: true, 
					active: false
				});
			});
		</script>
		<style type="text/css">
			#imagen{
				width: 450px;
			}
		</style>
		
  </head>
	<body>
		<div id="myModal" class="modal modal1 fade">
    		<div class="modal-dialog modal-lg ">
        		<div class="modal-content gradient">
            		<div class="modal-header text-center">
                		<button type="button" class="close" onclick="cerrarPrimer()" aria-hidden="true">&times;</button>
                		<h2 class="modal-title">Promoción del Mes</h2>
            		</div>
            		<div class="modal-body row justify-content-center">
						<?php 
						$fil = $resmodal->num_rows;
							while ($re = $resmodal->fetch_assoc()){
								$hoy = date('Y-m-d', time()); 
								$prominicio = date('Y-m-d', strtotime($re['inicio']));
								$promfin = date('Y-m-d', strtotime($re['fin']));
								if (($hoy >= $prominicio)&& ($hoy <= $promfin)){
									if ($fil == 1){
										echo'<div class="col-12 col-sm-12 col-md-12 master1a"><div class="card carta" data-id="'.$re['id'].'" data-titulo="'.$re['titulo'].'" data-descripcion="'.$re['descripcion'].'" data-disclaimer="'.$re['disclaimer'].'" data-imagen="'.$re['imagen'].'?'.$current_date.'" data-inicio="'.$re['inicio'].'" data-fin="'.$re['fin'].'" onclick="functionMostrarModal()" style="cursor: pointer">';
									}else{
										echo'<div class="col-12 col-sm-6 col-md-6 master1a"><div class="card carta" data-id="'.$re['id'].'" data-titulo="'.$re['titulo'].'" data-descripcion="'.$re['descripcion'].'" data-disclaimer="'.$re['disclaimer'].'" data-imagen="'.$re['imagen'].'?'.$current_date.'" data-inicio="'.$re['inicio'].'" data-fin="'.$re['fin'].'" onclick="functionMostrarModal()" style="cursor: pointer">';
									}
									echo'<img class="card-img-top" src="miscelaneos/imgs/'.$re['imagen'].'?'.$current_date.'" alt="" style="maxHeight: 100px; maxWidth: 100px;">';
									echo'<div class="card-body">';
									echo '<h3>'.$re['titulo'].'</h3>';
									echo'</div></div></div>';
								}
							}
						?>
        			</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" onclick="cerrarPrimer()"><h3>Cerrar</h3></button>
					</div>
    			</div>
			</div>
		</div>
		<div id="myModal2" class="modal fade">
    		<div class="modal-dialog modal-lg">
        		<div class="modal-content gradient text-center">
            		<div class="modal-header">
                		<button type="button" class="close" onclick="cerrarSegund()" aria-hidden="true" >&times;</button>
                		<h2 class="modal-title text-center">Promoción del Mes</h2>
            		</div>
            		<div class="modal-body row justify-content-center ">
            			
            			<div class="col-12">
            				<img id="imagen" name="imagen" class="img-fluid">
            			</div>
            			
            			<div class="col-12">
            				<hr>
            				<h1 name="titulo" id="titulo"></h1>
            			</div>
            			<div class="col-12">
            				<h3 name="descripcion" id="descripcion"> </h3>
            			</div>
            			<div class="col-12">
            				<tagred name="disclaimer" id="disclaimer"></tagred>
            			</div>
            			<div class="row col-12">
            				<div class="col-md-6">
            					<h4 name="inicio" id="inicio"></h4>
            				</div>
            				<div class="col-md-6">
            					<h4 name="fin" id="fin"></h4>
            				</div>
            			</div>
            		</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" onclick="cerrarSegund()"><h3>Cerrar</h3></button>
					</div>
            	</div>
            </div>
        </div>
		<?php require_once('./link/nav.php');?>
		<section class="supertop">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-12 col-sm-10 col-md-8 text-center">
						<hr>
						<h2>Búsqueda de Productos NV</h2>
						<br>
						<form action="productos">
							<div class="form-group">
								<input type="text" class="form-control" id="txtnombreprod" name="txtnombreprod" placeholder="Búsqueda de Productos" required>
								<input type="hidden" value="1" name="formController">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-nv1">Buscar <i class="fa fa-search" aria-hidden="true"></i></button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<br>
			<div class="container-fluid ">
				<div class="row justify-content-center">
					<div class="col-12 col-sm-11 col-md-10 text-center">
						<hr>
						<h2>Promociones NV</h2>
						<br>
						<div class="row justify-content-center">
						<?php 
							while ($prom = $resultprom->fetch_assoc()){
								$hoy = date('Y-m-d', time()); 
								$prominicio = date('Y-m-d', strtotime($prom['inicio']));
								$promfin = date('Y-m-d', strtotime($prom['fin']));
								if (($hoy >= $prominicio)&& ($hoy <= $promfin)){
									echo'<div class="col-12 col-md-6 col-lg-4 master1a"><div class="card">';
									echo'<a data-lightbox="image-1" href="miscelaneos/imgs/'.$prom['imagen'].'"><img class="card-img-top" src="miscelaneos/imgs/'.$prom['imagen'].'?'.$current_date.'" alt=""></a>';
									echo'<div class="card-body">';
									echo '<h3>'.$prom['titulo'].'</h3>';
									if (!empty($prom['descripcion'])){
										echo'<p class="card-text">'.$prom['descripcion'].'</p>';
										echo'<a href="miscelaneos/showPromo.php?id='.$prom['id'].'" class="btn btn-nv1">Más Información <i class="fa fa-info-circle" aria-hidden="true"></i></a>';
									
									}
									echo '</div></div></div>';
								}
							}
						?>
						</div>
						<hr>
					</div>
				</div>
			</div>
			<br>
			<div class="container-fluid row">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center">
						<h2>Datos del Usuario</h2>
						<br>
						<table class="table table-striped table-bordered table-hover"> 				
							<tr><td><b>Nombre:</b></td><td><?php echo $row['nombre'].' '.$row['apellido']?></td></tr>
							<tr><td><b>Username:</b></td><td><?php echo $row['username']?></td></tr>
							<tr><td><b>Email:</b></td><td><a href="mailto:<?php echo $row['correo']?>"><?php echo $row['correo']?></a></td></tr>
							<tr><td><b>Último Ingreso:</b></td><td> <?php echo $row['ultimaModificacion'] ?> </td></tr>
							<?php
							if (isset($row['puntos'])){
								echo "<tr><td><b>Puntos :</b></td><td>".$row['puntos']."</td></tr>";
							}
							?>
						</table>
					</div>
			<br>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center">
						<h2>Documentos NV</h2>
						<br>
					<div id="accordion">
						<?php
							$query = "SELECT tipo FROM tipodocumentos";
							$docs = $conn->query($query);
							$resultado = [];
							while($varia = $docs->fetch_assoc()){
								$resultado[] = $varia;
							}
							foreach ($resultado as $key => $va) {
								$que = "SELECT nombre, titulo FROM documentos WHERE tipo = '".$va['tipo']."' AND estado = 'activo'";
								$do = $conn->query($que);
								$ro = $do->num_rows;
								$res = [];
								while($varia2 = $do->fetch_assoc()){
									$res[] = $varia2;
								}
								if($ro > 0){
									echo "<h3>";
									echo $va['tipo']."</h3><div>";
									foreach ($res as $valor => $documento) {
										$docu = pathinfo($documento['titulo'], PATHINFO_FILENAME);
										echo "<p><a href='miscelaneos/documentos/data/".$documento['nombre']."' target='_blank'>".$docu."</a></p>";
									}
									echo "</div>";
								}
							}
							?>
						</table>
					</div>
				</div>
				</div>
		</section>
		<?php require_once('./link/footer.php');?>
		<?php require_once('./function/modal.php');?>
  		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/lightbox.js"></script>
		<script type="text/javascript">
			function functionMostrarModal(){
				$("#myModal2").modal({
        			backdrop: 'static',
        			keyboard: false,
					overflow: 'hidden'
        		});
				$("#myModal2").modal('show');
				//$("#myModal").modal('hide');
			};

			function cerrarPrimer(){
				$("#myModal").modal('hide');
			}
			function cerrarSegund(){
				$("#myModal2").modal('hide');
			}
			$(document).ready(function(){
        		$("#myModal").modal({
        			backdrop: 'static',
        			keyboard: false,
					focus: true
        		});
        		$("#myModal").modal('show');
    		});
			$(document).on("click", ".carta", function(){
				var idProm = $(this).data('id');
				var tituloProm = $(this).data('titulo');
				var descripProm = $(this).data('descripcion');
				var disclaProm = $(this).data('disclaimer');
				var imaProm = $(this).data('imagen');
				var iniProm = $(this).data('inicio');
				var finProm = $(this).data('fin');
				$("#myModal2 #titulo").empty().append(tituloProm);
				$("#myModal2 #descripcion").empty().append(descripProm);
				$("#myModal2 #disclaimer").empty().append(disclaProm);
				$("#myModal2 #inicio").empty().append("Fecha de Inicio: "+iniProm);
				$("#myModal2 #fin").empty().append("Fecha Final: "+finProm);
				$("#myModal2 #imagen").attr("src", "miscelaneos/imgs/"+imaProm);



			});
			$('#myModal2').on('hidden.bs.modal', function (e) {
      			$('body').addClass('modal-open');
    		});

		</script>
		<script type="text/javascript">
	  		lightbox.option({
	  			'maxWidth': 800,
	  			'maxHeight': 800,
	  			'showImageNumberLabel': false,
	  		})	
  		</script>
	</body>
</html>
