<?php 

?>
<div class="modal fade" id="modPrincipal" tabindex="-1" role="dialog" aria-labelledby="modPrincipalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-center" id="modPrincipalLabel">Promoción del día</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img src="http://placehold.it/1200x800" alt="" class="img-fluid d-none d-md-block">
				<img src="http://placehold.it/800x1000" alt="" class="img-fluid d-block d-md-none">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>