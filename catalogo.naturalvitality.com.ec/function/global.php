<?php

session_start([
	'cookie_lifetime' => 3600,
]);

define('DEBUG',true);
define('SESSIONV',false);
define('TESTDB',true);
define('PRUEBAS', false);
define('TESTUR',true);

date_default_timezone_set('America/Guayaquil');
$current_date = date('d/m/Y == H:i:s');
if (PRUEBAS){
	define('ROOT_PATH_HTML',$_SERVER['DOCUMENT_ROOT'].'/e-channel/catalogo.naturalvitality.com.ec/');
	define('ROOT_PATH_PHP', $_SERVER['SERVER_NAME'].'/e-channel/catalogo.naturalvitality.com.ec/');
}else{
	define('ROOT_PATH_HTML',$_SERVER['DOCUMENT_ROOT']."/");
	define('ROOT_PATH_PHP', $_SERVER['SERVER_NAME']."/");
}


if (DEBUG){
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
}

if (SESSIONV){
	$sid = md5('catalogo.nv');
	session_id($sid);
	// session_start([
	// 	'cookie_lifetime' => 1800,
	// ]);
	session_start([
		'cookie_lifetime' => 14400,
	]);
	$_SESSION['ACT'] = true;
}

?>
