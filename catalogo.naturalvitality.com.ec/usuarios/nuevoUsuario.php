<?php 
require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'function/redirect.php');
require_once(ROOT_PATH_HTML.'link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Usuarios';
$_SESSION['ubix'] = 4;
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'link/meta.php');?>
		<title>Catálogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/datepicker.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


	</head>
	<body>
		<?php require_once(ROOT_PATH_HTML.'link/nav.php');?>
		<section class="supertop">
			<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-10 col-sm-10 col-md-8">
					<h4 class="text-center">Nuevo Usuario</h4>
					<br>
					<form method="POST">
						<div class="row">
							<div class="form-group col-6">
								<label for="txtnombreusuario"> Nombre </label>
								<input type="text" class="form-control" id="txtnombreusuario" name="txtnombreusuario" placeholder="Nombres del Usuario" pattern="[a-zA-Záéíóú\s]+" title="Solo letras" required>
								<input type="hidden" id="formnuevousuario" name="formnuevousuario" value="1">
							</div>
							<div class="form-group col-6">
								<label for="txtapellidousuario"> Apellido </label>
								<input type="text" class="form-control" id="txtapellidousuario" name="txtapellidousuario" placeholder="Apellidos del Usuario" pattern="[a-zA-Záéíóú\s]+" required>
							</div>
						</div>

						<div class="row">
						<div class="form-group col-12">
							<label for="txtcorreo"> Correo electrónico </label>
							<input type="text" class="form-control" id="txtcorreo" name="txtcorreo" placeholder="E-mail" required>
						</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label for="txtcedulausuario"> Cédula </label>
								<input type="text" class="form-control" id="txtcedulausuario" name="txtcedulausuario" placeholder="Número de Cédula" required>
							</div>
							<div class="form-group col-6">
								<label for="txtpuesto"> Puesto </label>
								<select class="custom-select mb-2 mr-sm-2 mb-sm-0 form-control" id="txtpuesto" name="txtpuesto" required>
									<?php
										$query = "Select puesto from puesto;";
										$result = $conn->query($query);
										$fila = [];
										while($varia = $result->fetch_assoc()){
											$fila[] = $varia;
										}

										foreach ($fila as $pu) {
											echo "<option>".$pu['puesto']."</option>\n";
										}
									?>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label for="fechaingreso"> Fecha Ingreso </label>
								<input class="form-control" id="datepickerIngreso" name="fechaingreso" placeholder="Fecha de Ingreso">
							</div>
							<div class="form-group col-6">
								<label for="fechacumple"> Fecha Cumpleaños </label>
								<input type="text" class="form-control" id="datepickerCumple" name="fechacumple" placeholder="Fecha de Cumpleaños">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label for="txtusername"> Username </label>
								<input type="text" class="form-control" id="txtusername" name="txtusername" placeholder="Username a utilizarse por el usuario" pattern="[a-zA-Z]+" required>
							</div>
							<div class="form-group col-6">
								<label for="tipousuario"> Tipo de Usuario </label>
								<select class="custom-select mb-2 mr-sm-2 mb-sm-0 form-control" id="tipousuario" name="tipousuario" required>
									<option selected="selected"> Usuario Regular</option>
									<option> Administrador </option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label for="txtpasswd1"> Contraseña </label>
								<input type="password" class="form-control" id="txtpasswd1" name="txtpasswd1" placeholder="Contraseña" pattern="[\w]+">
							</div>
							<div class="form-group col-6">
								<label for="txtpasswd2"> Confirmar Contraseña </label>
								<input type="password" class="form-control" id="txtpasswd2" name="txtpasswd2" placeholder="Confirmar Contraseña" pattern="[\w]+">
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-success ">Registrar <i class="fa fa-pencil" aria-hidden="true"></i></button>
							<a href="../" class="btn btn-danger">Cancelar  <i class="fa fa-window-close" aria-hidden="true"></i></a>
						</div>
					</form>
					<?php
						if(isset($_POST['formnuevousuario'])){

							if($_POST['txtpasswd1'] == $_POST['txtpasswd2']){

								$nombreUsuario = $_POST['txtnombreusuario'];
								$apellidoUsuario = $_POST['txtapellidousuario'];
								$cedulaUsuario = $_POST['txtcedulausuario'];
								$correoUsuario = $_POST['txtcorreo'];
								$puestoUsuario = $_POST['txtpuesto'];
								$fechaIngreso= $_POST['fechaingreso'];
								$fechaCumple = $_POST['fechacumple'];
								$username= $_POST['txtusername'];
								$passwd = $_POST['txtpasswd1'];
								if($_POST['tipousuario'] == "Administrador"){
									$tipoUsuario = 2;
								}else{
									$tipoUsuario = 1;
								}
							}else{
								echo "Las contraseñas no son iguales";
							}
							$sql = $conn->prepare("Insert into users (nombre, apellido, cedula, correo, puesto, fechaingreso, username, passwd, cumpleanos, tipoUsuario, ultimaModificacion, creacionRegistro, estadoUsuario) values (?,?,?,?,?,STR_TO_DATE(?, '%d-%m-%Y'),?,?,STR_TO_DATE(?, '%d-%m-%Y'),?,CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'activo')");
							$sq = $conn->prepare("Insert into puntosverdes (username, ultimaModificacion, creacionRegistro) values (?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);");
							
							$sql->bind_param("sssssssssi", $nombreUsuario, $apellidoUsuario, $cedulaUsuario, $correoUsuario, $puestoUsuario, $fechaIngreso, $username, $passwd, $fechaCumple, $tipoUsuario);
							$sq->bind_param("s", $username);
							if ($sql->execute()){
								if ($sq->execute()){
								echo "<script>
								alert('Usuario creado exitosamente');
								window.location.href='//".ROOT_PATH_PHP."usuarios'
								</script>";
								}else{
									echo "Incorrecto";
								}
							}else{
								echo "Incorrecto";
							}
						}
					?>

				</div>
			</div>
		</div>
		</section>
		<?php require_once(ROOT_PATH_HTML.'link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/datepicker.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script>
  			$( function() {
    			$( "#datepickerIngreso" ).datepicker({
      				changeMonth: true,
      				changeYear: true,
      				dateFormat: 'dd-mm-yy'
    			});
  			} );
  			$( function() {
    			$( "#datepickerCumple" ).datepicker({
      				changeMonth: true,
      				changeYear: true,
      				dateFormat: 'dd-mm-yy'
    			});
  			} );
  			var password = document.getElementById("txtpasswd1");
  			var confirm_password = document.getElementById("txtpasswd2");

			function validatePassword(){
			  if(password.value != confirm_password.value) {
			    confirm_password.setCustomValidity("Las contraseñas no son iguales");
			  } else {
			    confirm_password.setCustomValidity('');
			  }
			}

			password.onchange = validatePassword;
			confirm_password.onkeyup = validatePassword;
  		</script>

	</body>
</html>
