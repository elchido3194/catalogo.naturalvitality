<?php 
require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'/function/redirect.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Miscelaneos';
$_SESSION['ubix'] = 4;
$username = '';
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catálogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	</head>
	<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');
			$displayform = "style='display:none'";
		?>
		<section class="supertop">
			<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12 col-sm-10 col-md-8">
					<h4 class="text-center">Modificar Usuario</h4>
					<div id="busqueda">
					<form id="formbusqueda">
							<div class="form-group">
								<label for="tags">Búsqueda</label>
								<input type="text" class="form-control" id="tags" name="username" placeholder="Búsqueda por Username">
							</div>
							<div class="form-group">
								<button type="submit" id="buttonBuscar" class="btn btn-nv">Buscar <i class="fa fa-search" aria-hidden="true"></i></button>
							</div>
					</form>
					</div>
					<?php
						if (isset($_GET['username'])){
							$username = $_GET['username'];
							$sql = "Select nombre, apellido, cedula, correo, puesto, DATE_FORMAT(fechaingreso, '%d-%m-%Y') as fechaingreso, username, passwd, DATE_FORMAT(cumpleanos, '%d-%m-%Y') as cumpleanos, administracion, tipoUsuario from users where username = '".$username."';";
							$result = $conn->query($sql);
							$rows = $result->num_rows;
							$row = $result->fetch_assoc();
							if($rows > 0){
								$displayform = "";
								$username = $row['username'];
							}else{
								echo "Usuario no encontrado";
							}
						}else{
							$row = array(
							    'nombre'  => "",
							    'apellido'  => "",
							    'cedula'  => "",
							    'correo' => "",
							    'puesto'  => "",
							    'fechaingreso'  => "",
							    'username' => "",
							    'passwd' => "",
							    'cumpleanos'  => "",
							    'administracion' => "",
							    'tipousuario'  => ""
							);
							$uregular = "";
							$uadmin = "";
						}
					?>
					<div id="formulario" <?php echo $displayform; ?>>
					<form method="POST">
						<div class="row">
							<div class="form-group col-6">
								<label for="txtnombreusuario"> Nombre </label>
								<input type="text" class="form-control" id="txtnombreusuario" name="txtnombreusuario" placeholder="Nombre" pattern="[a-zA-Záéíóú\s]+" title="Solo letras" value="<?php echo $row['nombre']; ?>">
								<input type="hidden" id="formnuevousuario" name="formnuevousuario" value="1">
							</div>
							<div class="form-group col-6">
								<label for="txtapellidousuario"> Apellido </label>
								<input type="text" class="form-control" id="txtapellidousuario" name="txtapellidousuario" placeholder="Apellido" pattern="[a-zA-Záéíóú\s]+" title="Solo letras" value="<?php echo $row['apellido']; ?>">
							</div>
						</div>

						<div class="form-group">
							<label for="txtcorreo"> Correo electrónico </label>
							<input type="text" class="form-control" id="txtcorreo" name="txtcorreo" placeholder="E-mail" value="<?php echo $row['correo']; ?>">
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label for="txtcedulausuario"> Cédula </label>
								<input type="text" class="form-control" id="txtcedulausuario" name="txtcedulausuario" placeholder="Número de Cédula" pattern="[0-9]+" value="<?php echo $row['cedula']; ?>">
							</div>
							<div class="form-group col-6">
								<label for="txtpuesto"> Puesto </label>
								<select class="custom-select mb-2 mr-sm-2 mb-sm-0 form-control" id="txtpuesto" name="txtpuesto">
									<?php
										$query = "Select puesto from puesto;";
										$result = $conn->query($query);
										$fila = [];
										while($varia = $result->fetch_assoc()){
											$fila[] = $varia;	
										}
										foreach ($fila as $pu) {
											if ($row['puesto']==$pu['puesto']){
												echo "<option selected='selected'>".$pu['puesto']."</option>\n";
											}else{
												echo "<option>".$pu['puesto']."</option>\n";
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label for="fechaingreso"> Fecha Ingreso </label>
								<input class="form-control" id="datepickerIngreso" name="fechaingreso" value="<?php echo $row['fechaingreso']; ?>">
							</div>
							<div class="form-group col-6">
								<label for="fechacumple"> Fecha Cumpleaños </label>
								<input type="text" class="form-control" id="datepickerCumple" name="fechacumple" value="<?php echo $row['cumpleanos']; ?>">
							</div>
						</div>
												<div class="row">
							<div class="form-group col-6">
								<label for="txtusername"> Username </label>
								<input type="text" class="form-control" id="txtusername" name="txtusername" placeholder="Username a utilizarse" value="<?php echo $row['username']; ?>">
							</div>
							<div class="form-group col-6">
								<label for="tipousuario"> Tipo de Usuario </label>
								<select class="custom-select mb-2 mr-sm-2 mb-sm-0 form-control" id="tipousuario" name="tipousuario">
									<?php
									$uregular = "";
									$uadmin = "";
									switch ($row['tipoUsuario']) {
										case '1':
											$uregular = "selected = 'selected'";
											break;
										case '2':
											$uadmin = "selected = 'selected'";
											break;
										default:
											break;
									}?>
									<option <?php echo $uregular;?>> Usuario Regular</option>
									<option <?php echo $uadmin;?>> Administrador </option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label for="txtpasswd1"> Contraseña </label>
								<input type="password" class="form-control" id="txtpasswd1" name="txtpasswd1" placeholder="Contraseña" pattern="[\w]+" value="<?php echo $row['passwd']; ?>">
							</div>
							<div class="form-group col-6">
								<label for="txtpasswd2"> Confirmar Contraseña </label>
								<input type="password" class="form-control" id="txtpasswd2" name="txtpasswd2" placeholder="Confirmar Contraseña" pattern="[\w]+" value="<?php echo $row['passwd']; ?>">
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-nv">Registrar <i class="fa fa-pencil" aria-hidden="true"></i></button>
							<a href="../" class="btn btn-danger">Cancelar  <i class="fa fa-window-close" aria-hidden="true"></i></a>
						</div>
					</form>
					</div>
					<?php
						if(isset($_POST['formnuevousuario'])){

							if($_POST['txtpasswd1'] == $_POST['txtpasswd2']){

								$nombreUsuario = $_POST['txtnombreusuario'];
								$apellidoUsuario = $_POST['txtapellidousuario'];
								$cedulaUsuario = $_POST['txtcedulausuario'];
								$correoUsuario = $_POST['txtcorreo'];
								$puestoUsuario = $_POST['txtpuesto'];
								$fechaIngreso= $_POST['fechaingreso'];
								$fechaCumple = $_POST['fechacumple'];
								$usernamenew= $_POST['txtusername'];
								$passwd = $_POST['txtpasswd1'];
								if($_POST['tipousuario'] == "Administrador"){
									$tipoUsuario = 2;
								}else{
									$tipoUsuario = 1;
								}
							}else{
								echo "Las contraseñas no son iguales";
							}
							$sql = $conn->prepare("Update users set nombre = ?, apellido = ?, cedula = ?, correo = ?, puesto = ?, fechaingreso = STR_TO_DATE(?, '%d-%m-%Y'), username = ?, passwd = ?, cumpleanos = STR_TO_DATE(?, '%d-%m-%Y'), administracion = ?, tipoUsuario = ? where username = ?");
							$sqlpv = $conn->prepare("Update puntosverdes set username = ? where username = ?");
							$sql->bind_param("ssssssssssis", $nombreUsuario, $apellidoUsuario, $cedulaUsuario, $correoUsuario, $puestoUsuario, $fechaIngreso, $usernamenew, $passwd, $fechaCumple, $administracion, $tipoUsuario, $username);
							$sqlpv->bind_param("ss", $usernamenew, $username);
							if ($sql->execute()){
								if ($sqlpv->execute()){
									if ($_SESSION['username']==$username){
										$_SESSION['username'] = $usernamenew;
									}
									echo "<script>
									alert('Usuario modificado exitosamente');
									window.location.href='//".ROOT_PATH_PHP."usuarios'
									</script>";
								}else{
									echo "Incorrecto";
								}
							}else{
								echo "Incorrecto";
							}
						}
					?>
				</div>
			</div>
		</div>
		</section>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/dropzone.js"></script>
		<script>
		$(function() {
        $("#tags").autocomplete({
            source: function( request, response ) {
				var term = request.term;
				$.getJSON( "users.php", request, function( data, status, xhr ) {
					var filtered = data.filter(function(carro){
						return carro.label.indexOf(term) !== -1;
					})
					
					response( filtered );
					});
				},
            minLength: 2,
			select: function (event, ui){
				$("#tags").val(ui.item.label);
       			//$("#formbusqueda").submit();
			}
        	});
    	});

    	$( function() {
    			$( "#datepickerIngreso" ).datepicker({
      				changeMonth: true,
      				changeYear: true,
      				dateFormat: 'dd-mm-yy'
    			});
  			} );
  			$( function() {
    			$( "#datepickerCumple" ).datepicker({
      				changeMonth: true,
      				changeYear: true,
      				dateFormat: 'dd-mm-yy'
    			});
  			} );
  			var password = document.getElementById("txtpasswd1");
  			var confirm_password = document.getElementById("txtpasswd2");

			function validatePassword(){
			  if(password.value != confirm_password.value) {
			    confirm_password.setCustomValidity("Las contraseñas no son iguales");
			  } else {
			    confirm_password.setCustomValidity('');
			  }
			}

			password.onchange = validatePassword;
			confirm_password.onkeyup = validatePassword;
    	</script>
	</body>
</html>
