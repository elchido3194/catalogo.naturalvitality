<?php
require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'/function/redirect.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Usuarios';
$_SESSION['ubix'] = 4;
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catálogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	</head>
	<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');
			$displayform = "style='display:none'";
		?>
		<section class="supertop">
			<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12 col-sm-10 col-md-8">
					<h4 class="text-center">Eliminar Usuario</h4>
					<form id="formbusqueda">
							<div class="form-group">
								<label for="tags">Búsqueda</label>
								<input type="text" class="form-control" id="tags" name="username" placeholder="Búsqueda por username">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-nv">Buscar <i class="fa fa-search" aria-hidden="true"></i></button>
							</div>
					</form>
					<?php

						if (isset($_GET['username'])){
							$username = $_GET['username'];
							$sql = "Select id, nombre, apellido, cedula, correo, puesto, fechaingreso, username, passwd, cumpleanos, tipoUsuario from users where username = '".$username."';";
							$result = $conn->query($sql);
							$rows = $result->num_rows;
							$row = $result->fetch_assoc();
							if($rows > 0){
								$displayform = "";
							}else{
								echo "Usuario no encontrado";
							}
						}else{
							$row = array(
							    'nombre'  => "",
							    'apellido'  => "",
							    'cedula'  => "",
							    'correo' => "",
							    'puesto'  => "",
							    'fechaingreso'  => "",
							    'username' => "",
							    'passwd' => "",
							    'cumpleanos'  => "",
							    'tipousuario'  => ""
							);
							$uregular = "";
							$uadmin = "";
						}
					?>
					<div id="formulario" <?php echo $displayform; ?>>
					<form method="POST" onsubmit="return confirm('Esta seguro que desea eliminar usuario?');">
						<div class="row">
							<div class="form-group col-6">
								<label for="txtnombreusuario"> Nombre </label>
								<input type="text" class="form-control" id="txtnombreusuario" name="txtnombreusuario" placeholder="Nombre" pattern="[a-zA-Záéíóú\s]+" title="Solo letras" value="<?php echo $row['nombre']; ?>" disabled>
								<input type="hidden" id="formnuevousuario" name="formnuevousuario" value="1">
							</div>
							<div class="form-group col-6">
								<label for="txtapellidousuario"> Apellido </label>
								<input type="text" class="form-control" id="txtapellidousuario" name="txtapellidousuario" placeholder="Apellido" pattern="[a-zA-Záéíóú\s]+" title="Solo letras" value="<?php echo $row['apellido']; ?>" disabled>
							</div>
						</div>

						<div class="form-group">
							<label for="txtcorreo"> Correo electrónico </label>
							<input type="text" class="form-control" id="txtcorreo" name="txtcorreo" placeholder="E-mail" value="<?php echo $row['correo']; ?>" disabled>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label for="txtcedulausuario"> Cédula </label>
								<input type="text" class="form-control" id="txtcedulausuario" name="txtcedulausuario" placeholder="Número de Cédula" pattern="[0-9]+" value="<?php echo $row['cedula']; ?>" disabled>
							</div>
							<div class="form-group col-6">
								<label for="txtpuesto"> Puesto </label>
								<input type="text" class="form-control" id="txtpuesto" name="txtpuesto" placeholder="Puesto" pattern="[a-zA-Záéíóú\s]+" value="<?php echo $row['puesto']; ?>" disabled>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label for="fechaingreso"> Fecha Ingreso </label>
								<input class="form-control" id="datepickerIngreso" name="fechaingreso" value="<?php echo $row['fechaingreso']; ?>" disabled>
							</div>
							<div class="form-group col-6">
								<label for="fechacumple"> Fecha Cumpleaños </label>
								<input type="text" class="form-control" id="datepickerCumple" name="fechacumple" value="<?php echo $row['cumpleanos']; ?>" disabled>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label for="txtusername"> Username </label>
								<input type="text" class="form-control" id="username" name="username" placeholder="Username a utilizarse" pattern="[a-zA-Z]+" value="<?php echo $row['username']; ?>" disabled>
								<input type="hidden" id="txtusername" name="txtusername" value="<?php echo $row['username']; ?>">
							</div>
							<div class="form-group col-6">
								<label for="tipousuario"> Tipo de Usuario </label>
								<select class="custom-select mb-2 mr-sm-2 mb-sm-0 form-control" id="tipousuario" name="tipousuario" disabled>
									<?php
									$uregular = "";
									$uadmin = "";
									switch ($row['tipoUsuario']) {
										case '1':
											$uregular = "selected = 'selected'";
											break;
										case '2':
											$uadmin = "selected = 'selected'";
											break;
										default:
											break;
									}?>
									<option <?php echo $uregular;?>> Usuario Regular</option>
									<option <?php echo $uadmin;?>> Administrador </option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<button type="submit" class="btn btn-warning">Eliminar Usuario <i class="fa fa-exclamation-triangle" aria-hidden="true"></i></button>
							<a href="../" class="btn btn-danger">Cancelar  <i class="fa fa-window-close" aria-hidden="true"></i></a>
						</div>
					</form>
					</div>
					<?php
						if(isset($_POST['formnuevousuario'])){
							if($_SESSION['username'] != $_POST['txtusername']){
								$id= $row['id'];
								$estadoUsuario = 'inactivo';

								$sql = $conn->prepare("Update users set estadoUsuario = ? where id = ?");
								$sql->bind_param("ss", $estadoUsuario, $id);
								if ($sql->execute()){
									echo "<script>
									alert('Usuario eliminado exitosamente');
									window.location.href='//".ROOT_PATH_PHP."usuarios'
									</script>";
								}else{
									echo "Incorrecto";
								}
							}else{
								echo "<script>
									alert('No se puede eliminar al mismo usuario');
									</script>";
							}
						}
					?>
				</div>
			</div>
		</div>
		</section>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script>
        $(function() {
        $("#tags").autocomplete({
            source: function( request, response ) {
				var term = request.term;
				$.getJSON( "users.php", request, function( data, status, xhr ) {
					var filtered = data.filter(function(carro){
						return carro.label.indexOf(term) !== -1;
					})
					response( filtered );
					});
				},
            minLength: 2,
            select: function (event, ui){
				$("#tags").val(ui.item.label);
       			$("#formbusqueda").submit();
			}
        	});
    	});

    	$( function() {
    			$( "#datepickerIngreso" ).datepicker({
      				changeMonth: true,
      				changeYear: true,
      				dateFormat: 'yy-mm-dd'
    			});
  			} );
  		$( function() {
    			$( "#datepickerCumple" ).datepicker({
      				changeMonth: true,
      				changeYear: true,
      				dateFormat: 'yy-mm-dd'
    			});
  		} );
  		var password = document.getElementById("txtpasswd1");
  		var confirm_password = document.getElementById("txtpasswd2");

		function validatePassword(){
			if(password.value != confirm_password.value) {
			    confirm_password.setCustomValidity("Las contraseñas no son iguales");
			  } else {
			    confirm_password.setCustomValidity('');
			  }
			}
		password.onchange = validatePassword;
		confirm_password.onkeyup = validatePassword;

    	</script>
	</body>
</html>
