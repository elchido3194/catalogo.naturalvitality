<?php

require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'/function/redirect.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Usuarios';
$_SESSION['ubix'] = 4;


?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catálogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/simplePagination.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	</head>
	<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');?>
		<section class="supertop">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-10">
						<h3>Usuarios</h3>
					</div>
					<div class="col-2">
						<a href='./nuevoUsuario.php' class='btn btn-nv' role='button' aria-disabled='true'>Nuevo Usuario <i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
					</div>
					
					<div class="col-12">
							<div class="form-group">
								<br>
								<input type="text" class="form-control" id="tags" onkeyup="myFunction()" name="username" placeholder="Búsqueda por Username">
							</div>
							<div class="form-group">
								<button type="submit" id="buttonBuscar" class="btn btn-nv">Buscar <i class="fa fa-search" aria-hidden="true"></i></button>
							</div>
							
					</div>
					<div class="col-12 master1a">
						<table id="myTable" class="table table-striped table-bordered table-hover text-center">
							<thead>
								<tr>
									<th>#</th>
									<th>Nombre</th>
									<th>Username</th>
									<th>Estado Usuario</th>
									<th>Cédula</th>
									<th>Puntos</th>
									<th colspan="2">Acciones</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$query = "Select users.id, nombre, apellido, users.username, estadoUsuario, cedula, puntos from users join puntosverdes on users.username = puntosverdes.username";
									$sql = $conn->query($query);
									while($row = $sql->fetch_assoc()){
										//print_r ($row);
										echo "<tr class='paginate'>
											<td>".$row['id']."</td>
											<td>".$row['nombre']. " ".$row['apellido']."</td>
											<td>".$row['username']."</td>
											<td>".$row['estadoUsuario']."</td>
											<td>".$row['cedula']."</td>
											<td>".$row['puntos']."</td>
											<td><a href='./modificarUsuario.php?username=".$row['username']."' class='btn btn-nv btn-sm' role='button' aria-disabled='true'>Modificar <i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></td>";
										if ($row['estadoUsuario']=='inactivo'){
											echo "
												<td><a href='./reactivarUsuario.php?username=".$row['username']."' class='btn btn-default btn-sm' role='button' aria-disabled='true'>Reactivar Usuario <i class='fa fa-repeat' aria-hidden='true'></i></a></td>";
										}else{
											echo "
												<td><a href='./eliminarUsuario.php?username=".$row['username']."' class='btn btn-danger btn-sm' role='button' aria-disabled='true'>Borrar <i class='fa fa-ban' aria-hidden='true'></i></a></td>
											";
										}
										echo "
											</tr>";
									}
								?>
							</tbody>
						</table>
						<div id="page-nav"></div>
					</div>
				</div>
			</div>

			<br>
		</section>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 		<script src="<?php echo $_SESSION['nivelcap'];?>js/jquery.simplePagination.js"></script>
		<script>
		$(function() {
        $("#tags").autocomplete({
            source: function( request, response ) {
				var term = request.term;
				$.getJSON( "users.php", request, function( data, status, xhr ) {
					var filtered = data.filter(function(carro){
						return carro.label.indexOf(term) !== -1;
					})
					response( filtered );
					});
				},
            minLength: 2,
            select: function (event, ui){
				$("#tags").val(ui.item.label);
       			//$("#formbusqueda").submit();
       			}
        	});
    	});
    	$(function() {
    		$("#tablausuarios").pagination({
        	cssStyle: 'light-theme'
    		});

    		// Grab whatever we need to paginate
   			var pageParts = $(".paginate");

		    // How many parts do we have?
		    var numPages = pageParts.length;
		    // How many parts do we want per page?
		    var perPage = 25;

		    // When the document loads we're on page 1
		    // So to start with... hide everything else
		    pageParts.slice(perPage).hide();
		    // Apply simplePagination to our placeholder
		    $("#page-nav").pagination({
		        items: numPages,
		        itemsOnPage: perPage,
		        cssStyle: "light-theme",
		        // We implement the actual pagination
		        //   in this next function. It runs on
		        //   the event that a user changes page
		        onPageClick: function(pageNum) {
		            // Which page parts do we show?
		            var start = perPage * (pageNum - 1);
		            var end = start + perPage;

		            // First hide all page parts
		            // Then show those just for our page
		            pageParts.hide()
		            .slice(start, end).show();
		       	}
    		});
		});
		function myFunction() {
		  // Declare variables
		  var input, filter, table, tr, td, i;
		  input = document.getElementById("tags");
		  filter = input.value.toUpperCase();
		  table = document.getElementById("myTable");
		  tr = table.getElementsByTagName("tr");

		  // Loop through all table rows, and hide those who don't match the search query
		  for (i = 0; i < tr.length; i++) {
		    td = tr[i].getElementsByTagName("td")[2];
		    if (td) {
		      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
		        tr[i].style.display = "";
		      } else {
		        tr[i].style.display = "none";
		      }
		    }
		  }
		};
		</script>
	</body>
</html>
