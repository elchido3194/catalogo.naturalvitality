<?php 
require_once('../function/global.php');
require_once(ROOT_PATH_HTML.'/function/redirect.php');
require_once(ROOT_PATH_HTML.'/link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 2;
$_SESSION['ubicacion'] = 'Miscelaneos';
$_SESSION['ubix'] = 4;
$username = '';
?>
<?php
	if(isset($_POST['reactivarform'])){
		$username = $_POST['txtusername'];
		$sql = $conn->prepare("Update users set estadoUsuario = 'activo' where username = ?");
		$sql->bind_param("s", $username);
		if ($sql->execute()){
			echo "<script>
				alert('Usuario Reactivado');
				window.location.href='//".ROOT_PATH_PHP."usuarios'
				</script>";
		}else{
			echo "<script>
				alert('Usuario no pudo ser reactivado. Por favor intentelo nuevamente');
				</script>";
		}
	}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once(ROOT_PATH_HTML.'/link/meta.php');?>
		<title>Catálogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	</head>
	<body>
		<?php require_once(ROOT_PATH_HTML.'/link/nav.php');
			$displayform = "style='display:none'";
		?>
		<section class="supertop">
			<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12 col-sm-10 col-md-8">
					<h4 class="text-center">Reactivar Usuario</h4>
					<?php
						if (isset($_GET['username'])){
							$username = $_GET['username'];
							$sql = "Select nombre, apellido, cedula, correo, puesto, DATE_FORMAT(fechaingreso, '%d-%m-%Y') as fechaingreso, username, passwd, DATE_FORMAT(cumpleanos, '%d-%m-%Y') as cumpleanos, administracion, tipoUsuario from users where username = '".$username."';";
							$result = $conn->query($sql);
							$rows = $result->num_rows;
							$row = $result->fetch_assoc();
							if($rows > 0){
								$displayform = "";
								$username = $row['username'];
							}else{
								echo "Usuario no encontrado";
							}	
						}else{
							$row = array(
							    'nombre'  => "",
							    'apellido'  => "",
							    'cedula'  => "",
							    'correo' => "",
							    'puesto'  => "",
							    'fechaingreso'  => "",
							    'username' => "",
							    'passwd' => "",
							    'cumpleanos'  => "",
							    'administracion' => "",
							    'tipousuario'  => ""
							);
							$uregular = "";
							$uadmin = "";
						}
					?>
					<div id="busqueda">
						<h1 class="text-center">¿Está seguro que desea reactivar al usuario <b><?php echo $row['username'];?></b>?</h1>
					</div>
					<div class="col-12 pt-5">
						<table class="table table-striped table-bordered text-center">
							<tr>
								<td> Nombres </td>
								<td> <?php echo $row['nombre'];?></td>
							</tr>
							<tr>
								<td> Apellidos </td>
								<td> <?php echo $row['apellido'];?></td>
							</tr>
							<tr>
								<td> Cédula </td>
								<td> <?php echo $row['cedula'];?></td>
							</tr>
							<tr>
								<td> Correo  </td>
								<td> <?php echo $row['correo'];?></td>
							</tr>
							<tr>
								<td> Username </td>
								<td> <?php echo $row['username'];?></td>
							</tr>
						</table>
					</div>
					<div id="formulario" <?php echo $displayform; ?>>
					<form method="POST">
						<div class="form-group text-center">
							<input type="hidden" name="reactivarform" value="1">
							<input type="hidden" name="txtusername" value="<?php echo $row['username'];?>">
							<button type="submit" class="btn btn-nv">Reactivar <i class="fa fa-repeat" aria-hidden="true"></i></button>
							<a href="../" class="btn btn-danger">Cancelar  <i class="fa fa-window-close" aria-hidden="true"></i></a>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		</section>
		<?php require_once(ROOT_PATH_HTML.'/link/footer.php');?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/dropzone.js"></script>
		<script>
		$(function() {
        $("#tags").autocomplete({
            source: function( request, response ) {
				var term = request.term;
				$.getJSON( "users.php", request, function( data, status, xhr ) {
					var filtered = data.filter(function(carro){
						return carro.label.indexOf(term) !== -1;
					})
					
					response( filtered );
					});
				},
            minLength: 2,
			select: function (event, ui){
				$("#tags").val(ui.item.label);
       			//$("#formbusqueda").submit();
			}
        	});
    	});

    	$( function() {
    			$( "#datepickerIngreso" ).datepicker({
      				changeMonth: true,
      				changeYear: true,
      				dateFormat: 'dd-mm-yy'
    			});
  			} );
  			$( function() {
    			$( "#datepickerCumple" ).datepicker({
      				changeMonth: true,
      				changeYear: true,
      				dateFormat: 'dd-mm-yy'
    			});
  			} );
  			var password = document.getElementById("txtpasswd1");
  			var confirm_password = document.getElementById("txtpasswd2");

			function validatePassword(){
			  if(password.value != confirm_password.value) {
			    confirm_password.setCustomValidity("Las contraseñas no son iguales");
			  } else {
			    confirm_password.setCustomValidity('');
			  }
			}

			password.onchange = validatePassword;
			confirm_password.onkeyup = validatePassword;
    	</script>
	</body>
</html>
