<?php
require_once('./function/global.php');
require_once('./link/db.php');
$_SESSION['hora'] = date("Y-m-d H:i:s");
$_SESSION['retro'] = 1;
$_SESSION['ubicacion'] = 'Dashboard';
$_SESSION['ubix'] = 1;
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php require_once('./link/meta.php');?>
		<title>Catálogo NV - <?php echo $_SESSION['ubicacion'];?> - Usuario: <?php echo $_SESSION['username'];?></title>

		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/main.css">
		<link rel="stylesheet" href="<?php echo $_SESSION['nivelcap'];?>css/lightbox.css">

		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<style type="text/css">
			#imagen{
				width: 450px;
			}
		</style>
  </head>
	<body>
		<section class="supertop">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-12 col-sm-10 col-md-8 text-center">
					<a href="//<?php echo ROOT_PATH_PHP?>"><img src="//<?php echo ROOT_PATH_PHP ?>img/logo/logosmall.png">
					</a>
						<hr>
						<h2>Recuperación de Contraseña</h2>
						<br>
						<form method="POST">
							<div class="form-group">
                                <label for="txtusuario">Username</label>
								<input type="text" class="form-control" id="txtusuario" name="txtusuario" placeholder="Favor ingrese el username" required>
								<input type="hidden" value="1" name="formController">
                                <label for="correo">Correo Electrónico</label>
								<input type="text" class="form-control" id="correo" name="correo" placeholder="Favor ingrese el correo electrónico" required>
							</div>
							<div class="form-group">
								<label> Se le enviará un correo electronico con su clave </label>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-nv">Enviar <i class="fa fa-envelope-o" aria-hidden="true"></i></button>
							</div>
						</form>
						<?php
							if (isset($_POST['formController'])){
								$usuario = $_POST['txtusuario'];
								$correo = $_POST['correo'];
								$query = "SELECT username, passwd, correo FROM users where username = '$usuario' and correo = '$correo'";
								$sql = $conn->query($query);
								$rows = $sql->num_rows;
								$row = $sql->fetch_assoc();
								if ($rows > 0){
									$msg = "Catalogo Natural Vitality\n Usuario: $usuario \n Clave: ".$row['passwd']."\n Mensaje generado de forma automatica.\n";
									mail($correo, "Recuperacion Contrasena Natural Vitality", $msg);
									echo "Se ha enviado un correo a ".$correo." con instrucciones para recuperar su clave";
								}else{
									echo "<p> Usuario no encontrado </p>";
								}

							}
						?>
					</div>
				</div>
				<div class="col-12 text-right">
					<a href='../' class='btn btn-danger btn-sm' role='button' aria-disabled='true'>Regresar <i class='fa fa-reply' aria-hidden='true'></i></a>
				</div>
			</div>
			<br>
		</section>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/main.js"></script>
		<script src="<?php echo $_SESSION['nivelcap'];?>js/lightbox.js"></script>
		<script type="text/javascript">
			function functionMostrarModal(){
				$("#myModal2").modal({
        			backdrop: 'static',
        			keyboard: false,
					overflow: 'hidden'
        		});
				$("#myModal2").modal('show');
				//$("#myModal").modal('hide');
			};

			function cerrarPrimer(){
				$("#myModal").modal('hide');
			}
			function cerrarSegund(){
				$("#myModal2").modal('hide');
			}
			$(document).ready(function(){
        		$("#myModal").modal({
        			backdrop: 'static',
        			keyboard: false,
					focus: true
        		});
        		$("#myModal").modal('show');
    		});
			$(document).on("click", ".carta", function(){
				var idProm = $(this).data('id');
				var tituloProm = $(this).data('titulo');
				var descripProm = $(this).data('descripcion');
				var disclaProm = $(this).data('disclaimer');
				var imaProm = $(this).data('imagen');
				var iniProm = $(this).data('inicio');
				var finProm = $(this).data('fin');
				$("#myModal2 #titulo").empty().append(tituloProm);
				$("#myModal2 #descripcion").empty().append(descripProm);
				$("#myModal2 #disclaimer").empty().append(disclaProm);
				$("#myModal2 #inicio").empty().append("Fecha de Inicio: "+iniProm);
				$("#myModal2 #fin").empty().append("Fecha Final: "+finProm);
				$("#myModal2 #imagen").attr("src", "miscelaneos/imgs/"+imaProm);



			});
			$('#myModal2').on('hidden.bs.modal', function (e) {
      			$('body').addClass('modal-open');
    		});

		</script>
		<script type="text/javascript">
	  		lightbox.option({
	  			'maxWidth': 800,
	  			'maxHeight': 800,
	  			'showImageNumberLabel': false,
	  		})	
  		</script>
	</body>
</html>