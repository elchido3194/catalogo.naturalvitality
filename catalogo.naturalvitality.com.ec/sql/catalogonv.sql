-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-11-2017 a las 22:29:51
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `catalogonv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puntosverdes`
--

SELECT  id_product, position, `value` from ps_feature as pf left join ps_feature_lang as pfl on pf.id_feature = pfl.id_feature left join ps_feature_product as pfp on pf.id_feature = pfp.id_feature join ps_feature_value as pfv on pfv.id_feature = pf.id_feature join ps_feature_value_lang as pfvl on pfvl.id_feature_value = pfp.id_feature_value where pfp.id_feature_value = pfv.id_feature_value and pfp.id_product = 1 and position = 1

CREATE TABLE (
  id_product int,
  position int,
  value varchar(255)
)
---------

UPDATE productos join ps_product_lang on productos.id = ps_product_lang.id_product
SET productos.descripcioncorta = ps_product_lang.description_short



CREATE TABLE promociones (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `nombre` varchar(100),
    `titulo` varchar(100),
    `descripcion` varchar(250),
    `disclaimer` varchar(100),
    `modal` ENUM('si', 'no'),
    `inicio` date,
    `fin` date,
    `user` varchar(100),
    `ultimaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `creacionRegistro` timestamp NOT NULL,
    `estado` ENUM('activo', 'inactivo')
  )


CREATE TABLE `documentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nombre` varchar(100) DEFAULT NULL,
  `tipo` varchar(100) DEFAULT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `estado` enum('activo','inactivo') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE documentos(
  `id` int not null AUTO_INCREMENT PRIMARY key,
    `nombre` varchar(100),
    `tipo` varchar(100),
    `titulo` varchar(100)
)

CREATE TABLE tipodocumentos(
`id` int NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`tipo` varchar( 100 )
)

CREATE TABLE puesto(
`id` int NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`puesto` varchar( 100 )
)

select description, description_short from ps_product_lang join productos on ps_product_lang.id_product = productos.id

ALTER TABLE USERS
ADD COLUMN estadoUsuario ENUM(`activo`, `inactivo`)

update users
set estadoUsuario = 'activo'

CREATE TABLE `puntosverdes` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `puntos` double DEFAULT NULL,
  `ultimaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creacionRegistro` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellido` varchar(100) DEFAULT NULL,
  `cedula` varchar(100) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `puesto` varchar(100) DEFAULT NULL,
  `fechaIngreso` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `passwd` varchar(100) DEFAULT NULL,
  `cumpleanos` date DEFAULT NULL,
  `tipoUsuario` int(11) DEFAULT NULL,
  `ultimaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creacionRegistro` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `puntosverdes`
--
ALTER TABLE `puntosverdes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cedula` (`cedula`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `puntosverdes`
--
ALTER TABLE `puntosverdes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `puntosverdes`
--
ALTER TABLE `puntosverdes`
  ADD CONSTRAINT `fk_puntosusers` FOREIGN KEY (`username`) REFERENCES `users` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
